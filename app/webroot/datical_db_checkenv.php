<?php
$is_windows = false;
if (strstr(PHP_OS,"WIN"))
	$is_windows = true;

function _fail($msg,$key) {
	global $hlp;

	echo "<span class=\"fail\">&nbsp;FAIL&nbsp;</span>&nbsp;";
	echo $msg . "<br>";
	if (!empty($key))
		echo "<span class=\"help\">" . $hlp[$key] . "</span><br>";
}

function _pass($msg,$key = null) {
	global $hlp;

	echo "<span class=\"pass\">&nbsp;PASS&nbsp;</span>&nbsp;";
	echo $msg . "<br>";
	if (!empty($key))
		echo "<span class=\"help\">" . $hlp[$key] . "</span><br>";
}

function _info($msg,$key = null) {
	global $hlp;

	echo "<span class=\"info\">&nbsp;INFO&nbsp;</span>&nbsp;";
	echo $msg . "<br>";
	if (!empty($key))
		echo "<span class=\"help\">" . $hlp[$key] . "</span><br>";
}

function _ok($msg,$key = null) {
	global $hlp;

	echo "<span class=\"ok\">&nbsp;INFO&nbsp;</span>&nbsp;";
	echo $msg . "<br>";
	if (!empty($key))
		echo "<span class=\"help\">" . $hlp[$key] . "</span><br>";
}

function _indent($msg,$key = null) {
	global $hlp;

	echo "<span class=\"ok\">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;";
	echo $msg . "<br>";
	if (!empty($key))
		echo "<span class=\"help\">" . $hlp[$key] . "</span><br>";
}

function dnToRdn($string) {
	$parts = str_replace(',','.',str_replace('dc=','',strtolower($string)));
	return($parts);
}

function getUserGroupList($ldaph,$attr,$dn,$seen = array()) {
	if(isset($seen[$dn]))
		return(array());
	$seen[$dn] = true;
	$results = array();
	$entries = @ldap_get_entries($ldaph,@ldap_search($ldaph,$dn,"(objectClass=*)"));
	if (!empty($entries['count'])) {
		for ($i = 0 ; $i < $entries['count'] ; $i++) {
			if (isset($entries[$i][$attr])) {
				for ($j = 0 ; $j < $entries[$i][$attr]['count'] ; $j++) {
					array_push($results,$entries[$i][$attr][$j]);	// the current dn
					foreach (getUserGroupList($ldaph,$attr,$entries[$i][$attr][$j],$seen) as $grp)
						array_push($results,$grp);
				}
			}
		}
	}
	return($results);
}
function predictParams($params) {
	$did_set = 0;
	if (function_exists('ldap_connect')) {
		if (!empty($params['auth_msad_hostname']) && empty($params['auth_msad_rdn'])) {
			$port = 389;
			if (!empty($params['active_directory_port']))
				$port = $params['active_directory_port'];

			$h = @ldap_connect($params['auth_msad_hostname'],$port);
			ldap_set_option($h, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($h, LDAP_OPT_REFERRALS, 0);
			if (($s = @ldap_read($h, "", "objectclass=*", array('namingcontexts','defaultnamingcontext'))) !== false) {
				$e = @ldap_get_entries($h, $s);
				if (!empty($e[0]['defaultnamingcontext'][0])) {
					$params['auth_msad_rdn'] = dnToRdn($e[0]['defaultnamingcontext'][0]);
					$did_set = 1;
				}
				else {
					if (!empty($e[0]['namingcontexts'][0])) {
						$params['auth_msad_rdn'] = dnToRdn($e[0]['namingcontexts'][0]);
						$did_set = 1;
					}
				}
			}
		}
	}
	if ($did_set)
		save_test_params($params);
	return($params);
}

function db_mysql($host,$user,$pass,$name) {
	if (function_exists("mysql_connect")) {
		_pass("PHP MySQL functions are present");
		if (($rc = @mysql_connect($host,$user,$pass)) === false) {
			_fail("Can't connect to mysql server: " . mysql_error(),'mysql_connect_fail');
		}
		else {
			_pass("Database login credentials verified");
			if (@mysql_select_db($name) === false)
				_fail("Can't select database [$name] on mysql server: " . mysql_error(),'mysql_select_fail');
			else
				_pass("Database connection validated");
			$res = mysql_fetch_assoc(mysql_query("SHOW VARIABLES LIKE 'max_allowed_packet';"));
			if($res['Value'] < 16777216)
				_fail('Value for max_allowed_packet (' . showBytes($res['Value'], 0) . ') is insufficient','max_allowed_packet');
			else
				_pass('max_allowed_packet setting');
				
			mysql_close($rc);
		}
	}
	else {
		_fail("The MySQL functions are not configured in PHP",'no_mysql_funcs');
	}
}

function db_oracle($host,$user,$pass,$name) {
	if (function_exists("oci_connect")) {
		_pass("PHP Oracle functions are present");
		if (($rc = @oci_connect($user,$pass,"$host/$name")) === false) {
			_fail("Can't connect to Oracle server: $host/$name" . oci_error(),'oracle_connect_fail');
		}
		else {
			_pass("Database login credentials verified");
			oci_close($rc);
		}
	}
	else {
		_fail("The Oracle functions are not configured in PHP",'no_oracle_funcs');
	}
}

function db_odbc($host,$user,$pass,$name) {
	if (function_exists("odbc_connect")) {
		_pass("PHP ODBC functions are present");
		if (($rc = odbc_connect($host,$user,$pass)) === false) {
			_fail("Can't connect to ODBC DSN: $host" . odbc_errormsg(),'odbc_connect_fail');
		}
		else {
			_pass("Database connection validated");
			odbc_close($rc);
		}
	}
	else {
		_fail("The MSSQL functions are not configured in PHP",'no_mssql_funcs');
	}
}

function sqlsrv_get_last_message($err_list) {
	$errStmt = '';
	foreach($err_list as $err) {
		if(strlen($errStmt))
			$errStmt .= "<br/>";
		$errStmt .= $err['message'] . ' ';
	}
	return($errStmt);
}

function db_mssql($host,$user,$pass,$name) {
	if (function_exists("sqlsrv_connect")) {
		_pass("PHP MSSQL functions are present");
		$connectionInfo = array("UID" => $user, "PWD" => $pass, "Database" => $name,'MultipleActiveResultSets' => 0,'ConnectionPooling' => 0);
		if (($rc = sqlsrv_connect("$host", $connectionInfo)) === false) {
			_fail("Can't connect to MSSQL server on $host: " . sqlsrv_get_last_message(sqlsrv_errors()),'mssql_connect_fail');
		}
		else {
			_pass("Database connection validated");
			sqlsrv_close($rc);
		}
	}
	else {
		_fail("The MSSQL functions are not configured in PHP",'no_mssql_funcs');
		return;
	}
}

function load_test_params() {
	$params = array(
		'auth' => '',
		'db_type' => '',
		'db_host'  => '',
		'db_user' => '',
		'db_pass' => '',
		'db_name' => '',
		'auth_msad_hostname' => '',
		'auth_msad_rdn' => '',
		'auth_msad_login' => '',
		'auth_msad_password' => '',
		'auth_ldap_host_url' => '',
		'auth_ldap_query_dn' => '',
		'auth_ldap_query_password' => '',
		'auth_ldap_test_username' => '',
		'auth_ldap_test_password' => '',
		'auth_ldap_search_base_dn' => '',
		'auth_ldap_group_attr' => '',
		'auth_ldap_user_key' => '',
		'bhost' => '',
		'mrw' => '',
	);

	$path = "/tmp/checkpoc.params";
	if (!file_exists($path)) {
		if (($f = fopen($path,"w")) === false)
			$params['param_ok'] = false;
		else {
			$params['param_ok'] = true;
			fclose($f);
		}
	}
	if (($f = fopen($path,"r")) !== false) {
		while (!feof($f)) {
			if (($buf = rtrim(fgets($f))) != '') {
				list($var,$val) = explode(':',$buf,2);
				$params[$var] = $val;
			}
		}
		fclose($f);
	}
	return($params);
}

function save_test_params($params) {
	$path = "/tmp/checkpoc.params";
	if (($f = fopen($path,"w")) !== false) {
		foreach ($params as $var => $val)
			if ($var != '')
				fprintf($f,"$var:$val\n");
		fclose($f);
	}
}

function dump_test_params($params) {
	echo "---------------------------------------<br>";
	foreach ($params as $var => $val) {
		echo "[$var] => [$val]<br>";
	}
}

function show_db_form_mysql() {
	global $INFO;
?>
	<form method="POST" name="dparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setdb">
	<table>
	<tr><td align="right">Hostname:</td><td><input name="db_host" type="text" size="40" value="<?php echo $INFO['db_host']; ?>"></td></tr>
	<tr><td align="right">Database:</td><td><input name="db_name" type="text" size="40" value="<?php echo $INFO['db_name']; ?>"></td></tr>
	<tr><td align="right">Login:</td><td><input name="db_user" type="text" size="40" value="<?php echo $INFO['db_user']; ?>"></td></tr>
	<tr><td align="right">Password:</td><td><input name="db_pass" type="password" size="40" value="<?php echo $INFO['db_pass']; ?>"></td></tr>
	<tr><td align="right"></td><td><input type="submit" value="Test Database Connection"></form></td></tr>
	</table>
<?php
}

function show_db_form_oracle() {
	global $INFO;
?>
	<form method="POST" name="dparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setdb">
	<table>
	<tr><td align="right">Hostname:</td><td><input name="db_host" type="text" size="40" value="<?php echo $INFO['db_host']; ?>"></td></tr>
	<tr><td align="right">SID:</td><td><input name="db_name" type="text" size="40" value="<?php echo $INFO['db_name']; ?>"></td></tr>
	<tr><td align="right">Login:</td><td><input name="db_user" type="text" size="40" value="<?php echo $INFO['db_user']; ?>"></td></tr>
	<tr><td align="right">Password:</td><td><input name="db_pass" type="password" size="40" value="<?php echo $INFO['db_pass']; ?>"></td></tr>
	<tr><td align="right"></td><td><input type="submit" value="Test Database Connection"></form></td></tr>
	</table>
<?php
}

function show_db_form_odbc() {
	global $INFO;
?>
	<form method="POST" name="dparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setdb">
	<table>
	<tr><td align="right">Datasource Name:</td><td><input name="db_host" type="text" size="40" value="<?php echo $INFO['db_host']; ?>"></td></tr>
	<tr><td align="right">Login:</td><td><input name="db_user" type="text" size="40" value="<?php echo $INFO['db_user']; ?>"></td></tr>
	<tr><td align="right">Password:</td><td><input name="db_pass" type="password" size="40" value="<?php echo $INFO['db_pass']; ?>"></td></tr>
	<tr><td align="right"></td><td><input type="submit" value="Test Database Connection"></form></td></tr>
	</table>
<?php
}

function show_db_form_mssql() {
	global $INFO;
?>
	<form method="POST" name="dparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setdb">
	<table>
	<tr><td align="right">Hostname:</td><td><input name="db_host" type="text" size="40" value="<?php echo $INFO['db_host']; ?>"></td></tr>
	<tr><td align="right">Database:</td><td><input name="db_name" type="text" size="40" value="<?php echo $INFO['db_name']; ?>"></td></tr>
	<tr><td align="right">Login:</td><td><input name="db_user" type="text" size="40" value="<?php echo $INFO['db_user']; ?>"></td></tr>
	<tr><td align="right">Password:</td><td><input name="db_pass" type="password" size="40" value="<?php echo $INFO['db_pass']; ?>"></td></tr>
	<tr><td align="right"></td><td><input type="submit" value="Test Database Connection"></form></td></tr>
	</table>
<?php
}

function show_auth_form_msad() {
	global $INFO;

	$INFO = predictParams($INFO);
?>
	<form method="POST" name="lparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setmsad">
	<table>
	<tr><td align="right">AD Server:</td><td><input name="auth_msad_hostname" type="text" size="30" value="<?php echo $INFO['auth_msad_hostname']; ?>"></td></tr>
	<tr><td align="right">AD RDN:</td><td><input name="auth_msad_rdn" type="text" size="100" value="<?php echo $INFO['auth_msad_rdn']; ?>"></td></tr>
	<tr><td align="right">Login:</td><td><input name="auth_msad_login" type="text" size="100" value="<?php echo $INFO['auth_msad_login']; ?>"></td></tr>
	<tr><td align="right">Password:</td><td><input name="auth_msad_password" type="password" size="100" value="<?php echo $INFO['auth_msad_password']; ?>"></td></tr>
	<tr><td></td><td><input type="submit" value="Set MSAD Parameters"></td></tr>
	</table>
	</form>
<?php
}

function show_auth_form_ldap() {
	global $INFO;
?>
	<form method="POST" name="lparam" action="checkpoc.php">
	<input type="hidden" name="cmd" value="setldap">
	<table>
	<tr><td align="right">Hostname URL:</td><td><input name="auth_ldap_host_url" type="text" size="30" value="<?php echo $INFO['auth_ldap_host_url']; ?>"></td></tr>
	<tr><td align="right">Query Bind DN:</td><td><input name="auth_ldap_query_dn" type="text" size="100" value="<?php echo $INFO['auth_ldap_query_dn']; ?>"></td></tr>
	<tr><td align="right">Query Bind DN Password:</td><td><input name="auth_ldap_query_password" type="password" size="20" value="<?php echo $INFO['auth_ldap_query_password']; ?>"></td></tr>
	<tr><td align="right">Base DN:</td><td><input name="auth_ldap_search_base_dn" type="text" size="100" value="<?php echo $INFO['auth_ldap_search_base_dn']; ?>"></td></tr>
	<tr><td align="right">Search Key:</td><td><input name="auth_ldap_user_key" type="text" size="30" value="<?php echo $INFO['auth_ldap_user_key']; ?>"></td></tr>
	<tr><td align="right">Test Login:</td><td><input name="auth_ldap_test_username" type="text" size="20" value="<?php echo $INFO['auth_ldap_test_username']; ?>"></td></tr>
	<tr><td align="right">Test Password:</td><td><input name="auth_ldap_test_password" type="password" size="20" value="<?php echo $INFO['auth_ldap_test_password']; ?>"></td></tr>
	<tr><td align="right">Group Attribute Name:</td><td><input name="auth_ldap_group_attr" type="text" size="30" value="<?php echo $INFO['auth_ldap_group_attr']; ?>"></td></tr>
	<tr><td></td><td><input type="submit" value="Set LDAP Parameters"></td></tr>
	</table>
	</form>
<?php
}



// the help texts that are displayed when an error occurs
$hlp = array(
	'no_php_funcs'		=> 'Make sure the LDAP extension for PHP is installed and enabled.',
	'no_mysql_funcs'	=> 'Make sure the MySQL extension for PHP is installed and enabled.',
	'no_mssql_funcs'	=> 'Make sure the SQL Server extension for PHP is installed and enabled.',
	'no_odbc_funcs'		=> 'Make sure the ODBC extension for PHP is installed and enabled.',
	'no_oracle_funcs'	=> 'The oci8 module is not enabled in PHP. Verify the oci8 extension is present and enabled.',
	'no_db2_funcs'		=> 'The DB/2 module is not enabled in PHP. Verify the DB/2 extension is present and enabled.',
	'odbc_connect_fail'	=> 'Make sure the ODBC DSN is configured correctly.',
	'odbc_select_fail'	=> 'Make sure the database is created and the specified DSN has full access granted to the database.',
	'mssql_connect_fail'	=> 'Make sure the SQL Server client is installed, the hostname is correct, and the specified login credentials are correct.',
	'mssql_select_fail'	=> 'Make sure the database is created and the specified login has full access granted to the database.',
	'mssql_textlimit' => 'The mssql.textlimit value in php configuration file ' . get_cfg_var('cfg_file_path') . ' must be set to at least 65536.',
	'mssql_textsize' => 'The mssql.textsize value in php configuration file ' . get_cfg_var('cfg_file_path') . ' must be set to at least 65536.',
	'mysql_connect_fail'	=> 'Make sure the login credentials are correct.',
	'mysql_select_fail'	=> 'Make sure the database is created and the specified login has full access granted to the database.',
	'oracle_connect_fail'	=> 'Unable to connect to the Oracle database. Verify the Oracle configuration, including any env variables in the httpd config.',
	'db2_connect_fail'	=> 'Unable to connect to the DB/2 database. Verify the DB/2 configuration.',
	'ldap_connect'		=> 'Check the LDAP host URL, it should be of the form ldap://hostname[:port].',
	'php_version'		=> 'PHP Must be version 5.3 or greater. You\'ll need to upgrade your PHP.',
	'b_primary'		=> 'Make sure the bridge service is running and there are no firewall issues.',
	'b_secondary'		=> 'Try running the bridge from a command prompt to see if there are any OS issues preventing it from starting.',
	'ht_access'		=> 'Add write permission for the web server to be able to write to the directory containing this script.',
	'no_zlib'		=> 'Install the Zlib PHP extension and make sure the PHP configuration is configured to load the extension.',
	'no_ldap'		=> 'The ldap_connect function appears to be missing.',
	'no_posix_kill'		=> 'The posix_kill function appears to be missing.',
	'no_proc_open'	=> 'The proc_open function appears to be missing.',
	'max_execution_time'	=> 'The php.ini setting of max_execution_time must be set to a minimum of 300 (to prevent detailed report pages from timing out).',
	'short_open_tag'	=> 'The php.ini setting of short_open_tag must be set to Off (enables proper rendering of XML content).',
	'mod_rewrite' 	=> 'The apache configuration file must be modified to load the rewrite_module modules/mod_rewrite.so.',
	'uri_handler'	=> 'The apache configuration file must identify *.php files as application/x-httpd-php.',
	'allow_override'	=> 'The apache configuration file must have AllowOverride set to All for the directory serving the Q application.',
	'zero_zend_alloc'	=> 'The environment variable USE_ZEND_ALLOC should be set to the value 0 if your php reports "zend_mm_heap corrupted" errors.',
	'memory_limit'		=> 'The php.ini setting of memory_limit must be set to a minimum of 512M (to support large report generation).',
	'max_allowed_packet'	=> 'The MySQL setting for max_allowed_packet should be set to a minimum of 16M.',
);

	$INFO = load_test_params();
	date_default_timezone_set('America/Chicago');
	if (!empty($_GET)) {
		switch ($_GET['cmd']) {
			case 'reset':
				foreach (array_keys($INFO) as $key) {
					if ($key != '')
						$INFO[$key] = '';
				}
				unlink("/tmp/checkpoc.params");
				unlink(".htaccess");
				unlink("checkpoc.html");
				break;
			case 'mrw':
				$INFO['mrw'] = 1;
				break;
		}
	}
	if (!empty($_POST)) {
		switch ($_POST['cmd']) {
			case 'setauth':
				$INFO['auth'] = $_POST['auth'];
				$INFO['auth_msad_hostname'] = '';
				$INFO['auth_msad_rdn'] = '';
				$INFO['auth_msad_login'] = '';
				$INFO['auth_msad_password'] = '';
				foreach (array_keys($INFO) as $key)
					if (strncmp($key,"auth_ldap_",5) == 0)
						$INFO[$key] = '';
				break;
			case 'setdbtype':
				$INFO['db_type'] = $_POST['db_type'];
				$INFO['db_host'] = '';
				$INFO['db_user'] = '';
				$INFO['db_pass'] = '';
				$INFO['db_name'] = '';
				break;
			case 'setdb':
				$INFO['db_host'] = $_POST['db_host'];
				$INFO['db_user'] = $_POST['db_user'];
				$INFO['db_pass'] = $_POST['db_pass'];
				$INFO['db_name'] = $_POST['db_name'];
				break;
			case 'setmsad':
				$INFO['auth_msad_hostname'] = $_POST['auth_msad_hostname'];
				$INFO['auth_msad_rdn']  = $_POST['auth_msad_rdn'];
				$INFO['auth_msad_login'] = $_POST['auth_msad_login'];
				$INFO['auth_msad_password']  = $_POST['auth_msad_password'];
				break;
			case 'setldap':
				$INFO['auth_ldap_host_url'] = $_POST['auth_ldap_host_url'];
				$INFO['auth_ldap_query_dn']  = $_POST['auth_ldap_query_dn'];
				$INFO['auth_ldap_query_password']   = $_POST['auth_ldap_query_password'];
				$INFO['auth_ldap_test_username'] = $_POST['auth_ldap_test_username'];
				$INFO['auth_ldap_test_password'] = $_POST['auth_ldap_test_password'];
				$INFO['auth_ldap_search_base_dn'] = $_POST['auth_ldap_search_base_dn'];
				$INFO['auth_ldap_group_attr'] = $_POST['auth_ldap_group_attr'];
				$INFO['auth_ldap_user_key'] = $_POST['auth_ldap_user_key'];
				break;
			default:
				break;
		}
	}

	save_test_params($INFO);

	$php_vars = ini_get_all();

?>
<HEAD>
 <STYLE type="text/css">
   .fail {
		font-family: courier;
		color: white;
		background-color: red;
		font-weight: bold;
	}
	.pass {
		font-family: courier;
		color: white;
		background-color: green;
		font-weight: bold;
	}
	.info {
		font-family: courier;
		color: black;
		background-color: yellow;
		font-weight: bold;
	}
	.ok {
		font-family: courier;
		color: black;
		font-weight: bold;
	}
	.help {
		color: black;
		text-decoration: italic;
		font-weight: bold;
	}
</STYLE>
</HEAD>
<h1>Datical DB Server Prerequsite Checker</h1>
This script will test this system to make sure it's configured properly for the Datical DB Installation.
<p>
<?php
if ($INFO['param_ok'] === false) {
	_fail('Unable to store test parameters in temporary file in directory ' . getcwd(),'ht_access');
	die;
}
?>
<a href="checkpoc.php?cmd=reset">Reset All Settings</a><br>
Session Info:&nbsp;
<?php
	echo "[Id=" . session_id() . "]&nbsp;";
	echo "[Module=" . session_module_name() . "]&nbsp;";
	echo "[Name=" . session_name() . "]&nbsp;";
	foreach (session_get_cookie_params() as $key => $value) {
		echo "[$key=$value]&nbsp;";
	}
	echo "[Save=" . session_save_path() . "]&nbsp;<br>";
	echo 'Extensions directory: ' . $php_vars['extension_dir']['local_value'];
?>

<hr size="1" noshade>
<h3>PHP Configuration</h3>

<?php
$ver = explode('.',phpversion());
if ($ver[0] == '5' && intval($ver[1]) > 2)
	_pass('PHP Version: ' . phpversion());
else
	_fail('PHP Version: ' . phpversion(),'php_version');

if (function_exists("gzopen"))
	_pass('Zlib extension is present');
else
	_fail('Zlib extension is not present','no_zlib');

if (function_exists("socket_create"))
	_pass('sockets functions are present');
else
	_fail('sockets functions are not present','no_sock');

$have_ldap = 0;
if (function_exists("ldap_connect")) {
	_pass('LDAP functions are present');
	$have_ldap = 1;
}
else
	_fail('LDAP functions are not present','no_ldap');

if (function_exists("proc_open"))
	_pass('proc_open function is present');
else
	_fail('proc_open function is not present','no_proc_open');

if (!$is_windows) {
	if (function_exists("posix_kill"))
		_pass('POSIX kill function is present');
	else
		_fail('POSIX kill function missing','no_posix_kill');
}

if(ini_get('max_execution_time') > 299)
	_pass('max_execution_time setting');
else
	_fail('Value of max_execution_time (' . ini_get('max_execution_time') . ') is insufficient','max_execution_time');

if(!ini_get('short_open_tag'))
	_pass('short_open_tag setting is off');
else
	_fail('Value of short_open_tag is not correct','short_open_tag');
if(getBytes(ini_get('memory_limit')) > getBytes("255M"))
	_pass('memory_limit setting');
else
	_fail('Value of memory_limit (' . ini_get('memory_limit') . ') is insufficient','memory_limit');

?>

<hr size="1" noshade>
<h3>Database Configuration</h3>

<table>
<tr>
	<td valign="top">
		<form method="POST" name="dparam" action="checkpoc.php">
		<input type="hidden" name="cmd" value="setdbtype">
		<table>
		<tr>
			<td>Database Type:</td>
			<td><select name="db_type">
				<option <?php if ($INFO['db_type'] == 'mysql') echo 'selected'; ?> value="mysql">MySQL
				<option <?php if ($INFO['db_type'] == 'oracle') echo 'selected'; ?> value="oracle">Oracle
				<option <?php if ($INFO['db_type'] == 'mssql') echo 'selected'; ?> value="mssql">MS SQL (Native)
				</select>
			</td>
			<td><input type="submit" value="Set Database">
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
</table>

<?php 
if (!empty($INFO['db_type'])) {
	switch($INFO['db_type']) {
		case 'mysql':
			show_db_form_mysql();
			if (!empty($INFO['db_host']))
				db_mysql($INFO['db_host'],$INFO['db_user'],$INFO['db_pass'],$INFO['db_name']);
			break;
		case 'odbc':
			show_db_form_odbc();
			if (!empty($INFO['db_host']))
				db_odbc($INFO['db_host'],$INFO['db_user'],$INFO['db_pass'],$INFO['db_name']);
			break;
		case 'mssql':
			show_db_form_mssql();
			if (!empty($INFO['db_host']))
				db_mssql($INFO['db_host'],$INFO['db_user'],$INFO['db_pass'],$INFO['db_name']);
			break;
		case 'oracle':
			show_db_form_oracle();
			if (!empty($INFO['db_host']))
				db_oracle($INFO['db_host'],$INFO['db_user'],$INFO['db_pass'],$INFO['db_name']);
			break;
	}
}
?>

<hr size="1" noshade>
<h3>Authentication Services</h3>

<table>
<tr>
	<td valign="top">
		<form method="POST" name="aparam" action="checkpoc.php">
		<input type="hidden" name="cmd" value="setauth">
		<table>
		<tr>
			<td>Auth Method:</td>
			<td><select name="auth">
				<option <?php if ($INFO['auth'] == 'none') echo 'selected'; ?> value="none">Builtin Database
				<option <?php if ($INFO['auth'] == 'msad') echo 'selected'; ?> <?php if (!$have_ldap) echo "disabled"; ?> value="msad">MS Active Directory
				<option <?php if ($INFO['auth'] == 'ldap') echo 'selected'; ?> <?php if (!$have_ldap) echo "disabled"; ?> value="ldap">LDAP
				</select>
			</td>
			<td><input type="submit" value="Set Method">
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
</table>

<?php 

if (!empty($INFO['auth'])) {
	switch($INFO['auth']) {
		case 'msad':
			show_auth_form_msad();
			if (!empty($INFO['auth_msad_hostname']))
				testMSADAuthConfig($INFO,$INFO['auth_msad_login'],$INFO['auth_msad_password']);
			break;
		case 'ldap':
			show_auth_form_ldap();
			if (!empty($INFO['auth_ldap_host_url']))
				testLDAPAuthConfig($INFO,$INFO['auth_ldap_test_username'],$INFO['auth_ldap_test_password']);
			break;
		case 'none':
			break;
	}
}

///////////////////////////////////////////////////////
///// end of checkpoc

function testMSADAuthConfig($ldap,$login = null,$passwd = null) {
	$results = array();
	$g = array();

	$ldap_results = array();
	if (function_exists('ldap_connect')) {
		if (!empty($ldap['auth_msad_hostname'])) {
			$ldap_handle = @ldap_connect($ldap['auth_msad_hostname']);

			if ($ldap_handle !== false) {
				_pass("Initialization successful");

				if (ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3) !== false) {
					ldap_set_option($ldap_handle, LDAP_OPT_REFERRALS, 0);
					_pass("Protocol Version 3 selected");

					if (!empty($login) && !empty($passwd)) {
						_indent("Testing connection with login: $login");
						if (strpos($login,"@") === false)
							$rdn_user = $login . "@" . $ldap['auth_msad_rdn'];
						else
							$rdn_user = $login;
	
						if (@ldap_bind($ldap_handle,$rdn_user,$passwd) !== false) {
							_pass("Bind successful using: " . $rdn_user);
	
							// break up the account name and rdn
							list($username,$domain) = explode('@',$rdn_user);
							$search_base = "dc=" . join(',dc=',explode('.',$domain));
							$account_key = "sAMAccountName";
							if (!empty($params['active_directory_account_key']))
								$account_key = $params['active_directory_account_key'];
							$filter = "$account_key=$login";
							$groups_key = 'memberof';
							if (!empty($params['active_directory_memberof_key']))
								$groups_key = $params['active_directory_memberof_key'];

							$attrs = array($account_key,'dn',$groups_key);

							_indent("Search Base: " . $search_base);
							_indent("Search Filter: " . $filter);
							foreach ($attrs as $key)
								_indent("Requesting attribute: $key");
							if (($srch = @ldap_search($ldap_handle,$search_base,$filter,$attrs)) !== false) {
								_pass( "User account search succeeded");
								if (($ldap_results = @ldap_get_entries($ldap_handle,$srch)) !== false) {
									_pass("Data retrieval successful");
									if ($ldap_results['count'] == 1) {
										_pass("Retrieved a unique account record");
										$ldap_user = $ldap_results[0];
										_pass("Authentication succeeded for user: " . $ldap_user['dn']);
										$memberOf = array();
										foreach ($ldap_user[$groups_key] as $dn) {
											if (is_string($dn)) {
												array_push($memberOf,$dn);
												foreach (getUserGroupList($ldap_handle,$groups_key,$dn) as $grp)
													array_push($memberOf,$grp);
											}
										}
										if (empty($ldap['active_directory_no_group_user']))
											array_push($memberOf,$username);
										if (count($memberOf)) {
											_pass("Group membership search results:");
											foreach ($memberOf as $dn)
												_indent($dn);
										}
										else
											_indent("Group membership search returned no results");
	
									}
									else
										_fail("Search returned zero or multiple matches");
								}
								else
									_fail("Unable to retrieve account record from server: " . ldap_error($ldap_handle));
							}
							else
								_fail("Search of directory repository failed: " . ldap_error($ldap_handle));
						}
						else
							_fail("Unable to bind $rdn_user: " . ldap_error($ldap_handle));
					}
				}
				else
					_fail("Unable to select LDAP protocol version 3: " . ldap_error($ldap_handle));
			}
			else
				_fail("Unable to connect to Active Directory server: " . ldap_error($ldap_handle));
			ldap_close($ldap_handle);
		}
		else
			_fail("Server hostname URL not specified");
	}
	else
		_fail("Server configuration error: LDAP PHP functions not present");
}

function testLDAPAuthConfig($ldap,$login = null,$password = null) {
	$results = array();
	$g = array();


	$ldap_results = array();
	if (function_exists('ldap_connect')) {
		if (!empty($ldap['auth_ldap_host_url'])) {
			$ldap_handle = @ldap_connect($ldap['auth_ldap_host_url']);

			if ($ldap_handle !== false) {
				_pass("Initialization successful");

				if (ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3) !== false) {
					_pass("Protocol Version 3 selected");

					if (@ldap_bind($ldap_handle,$ldap['auth_ldap_query_dn'],$ldap['auth_ldap_query_password']) !== false) {
						_pass("Bind successful using: " . $ldap['auth_ldap_query_dn']);
						if (!empty($login)) {
							if (empty($ldap['auth_ldap_user_key']))
								$params['auth_ldap_user_key'] = 'uid';
							$filter = $ldap['auth_ldap_user_key'] . "=$login";
							_pass("LDAP Search Base: " . $ldap['auth_ldap_search_base_dn']);
							_pass("LDAP Search Filter: " . $filter);
							if (($srch = @ldap_search($ldap_handle,$ldap['auth_ldap_search_base_dn'],$filter)) !== false) {
								_pass( "User account search succeeded");
								if (($ldap_results = @ldap_get_entries($ldap_handle,$srch)) !== false) {
									if ($ldap_results['count'] == 1) {
										_pass("Retrieved a unique account record");
										$ldap_user = $ldap_results[0];
										if (@ldap_bind($ldap_handle,$ldap_user['dn'],$password) !== false) {
											if (empty($password))
												array_push($results,$info . "Password for bind is empty");
											_pass("Authentication succeeded for user: " . $ldap_user['dn']);
											$memberOf = array();
											if (!empty($ldap['auth_ldap_groups_key'])) {
												$mokey = $ldap['auth_ldap_groups_key'];
												if (isset($ldap_user[$mokey])) {
													foreach ($ldap_user[$mokey] as $dn) {
														if (is_string($dn)) {
															array_push($memberOf,$dn);
															foreach (getUserGroupList($ldap_handle,$mokey,$dn) as $grp)
																array_push($memberOf,$grp);
														}
													}
												}
												else
													_fail("Group Membership Attribute not present");
											}
											if (empty($ldap['ldap_no_group_user']))
												array_push($memberOf,$login);
											if (count($memberOf)) {
												_pass("Group membership search results:");
												foreach ($memberOf as $dn)
													_indent($dn);
											}
											else
												_indent("Group membership search returned no results");
										}
										else
											_fail("Authentication failed: " . ldap_error($ldap_handle));
									}
									else
										_fail("Search returned zero or multiple matches");
								}
								else
									_fail("Unable to retrieve LDAP records from server: " . ldap_error($ldap_handle));
							}
							else
								_fail("Search of LDAP repository failed: " . ldap_error($ldap_handle));
						}
					}
					else
						_fail("Unable to bind using '" . $ldap['auth_ldap_query_dn'] . "': " . ldap_error($ldap_handle));
				}
				else
					_fail("Unable to select LDAP protocol version 3: " . ldap_error($ldap_handle));
			}
			else
				_fail("Unable to connect to LDAP server: " . ldap_error($ldap_handle));
			ldap_close($ldap_handle);
		}
		else
			_info("Server hostname URL not specified");
	}
	else
		_fail("Server configuration error: LDAP PHP functions not configured");
}

function getBytes($val) {
	$val = trim($val);
	$last = strtolower($val[strlen($val)-1]);
	switch($last) {
		// The 'G' modifier is available since PHP 5.1.0
		case 'g':
		$val *= 1024;
		case 'm':
		$val *= 1024;
		case 'k':
		$val *= 1024;
	}
	return $val;
}

	function showBytes($number, $dec = 1, $dec_char = '.', $thousands_char = ',') {
		if ($number == 0)
			return(0);
		$units = array('b', 'K', 'M', 'G', 'T');
		$unit = floor(log($number, 2) / 10);
		if($unit == 0) $dec = 0;
		return(number_format($number / pow(1024, $unit), $dec, $dec_char, $thousands_char).$units[$unit]);
	}

?>
