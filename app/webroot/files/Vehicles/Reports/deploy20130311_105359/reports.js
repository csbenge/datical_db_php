$(document).ready(function() {

  var expanded;

  // Expand Button Handler -----------------
  $('.expand-button').click(function(){
    var expandable = $(this).parent().find('.expandable');

    if ($(this).hasClass('expanded')) {
      expandable.hide('fast');
      $(this).removeClass('expanded');
      $(this).html('Expand Details');
    }
    else {
      expandable.show('fast');
      $(this).addClass('expanded');
      $(this).html('Collapse Details');
    }
  });

  // Expand All Handler ---------------------
  $('.expand-all-button').click(function(){

    if (expanded) {
      $('.expandable').hide('fast');
      $(this).val('Expand ALL Details');
      $('.expand-button').removeClass('expanded');
      $('.expand-button').html('Expand Details');
    }
    else {
      $('.expandable').show('fast');
      $(this).val('Collapse ALL Details');
      $('.expand-button').addClass('expanded');
      $('.expand-button').html('Collapse Details');
    }

    expanded = !expanded;
  });

  // If I.E. make tables fill full width;

});