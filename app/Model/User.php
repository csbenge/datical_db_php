<?php

// app/Model/User.php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel {
  
  var $belongsTo = array(
		'Role' => array(
				'className'    => 'Role',
				'foreignKey'    => 'role_id'
		)
	);

	var $hasMany = array(
		'Credential' => array(
				'className'    => 'Credential'
		)
	);

  
  public $validate = array(
    'username' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'A username is required.'
        ),
        array(
          'rule' => 'isUnique',
          'message' => 'This username is already taken.'
      )
    ),
    
    'password' => array(
      'required' => array(
          'rule' => array('notEmpty'),
          'message' => 'A password is required.'
      ),
      array(
        'rule' => array('minLength', 4),
        'message' => 'Password must be at least 4 characters.' 
      ),
      array(
      'rule' => array('passCompare'),
      'message' => 'The passwords do not match.'
      )
    )
    
  );
  
  public function passCompare() {
    return ($this->data[$this->alias]['password'] === $this->data[$this->alias]['password_confirm']);        
  }
  
  public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
      $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
    }
    return true;
  }
}
