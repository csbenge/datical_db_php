<?php

// app/Model/DatabaseReport.php

class DatabaseReport extends AppModel {
	
	public $useTable = 'db_reports';
	
	var $belongsTo = array(
		'Database' => array(
				'className'    => 'Database',
				'foreignKey'    => 'db_id'
		)
	);
	
	public $validate = array(
        'dbrep_name' => array(
            'rule' => 'notEmpty',
						'message' => 'A database report name is required.'
        ),
				'dbrep_type' => array(
          'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
        ),
				'dbrep_date' => array(
          'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
        ),
				'dbrep_data' => array(
            'rule' => 'notEmpty',
						'message' => 'A database password is required.'
        ),
				'db_id' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
				)
    );
	
}

