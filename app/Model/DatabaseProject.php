<?php

// app/Model/DatabaseProject.php

class DatabaseProject extends AppModel {
	
	public $useTable = 'db_projects';
	
	var $belongsTo = array(
		'User' => array(
				'className'    => 'User',
				'foreignKey'    => 'user_id'
			),
	);
	
	public $validate = array(
        'projectname' => array(
            'rule' => 'notEmpty',
						'message' => 'A project name is required.'
        ),
        'description' => array(
            'rule' => 'notEmpty',
						'message' => 'A description is required.'
        )
    );
	
	public function isOwnedBy($project, $user) {
        return $this->field('id', array('id' => $project, 'user_id' => $user)) === $project;
    }
}

