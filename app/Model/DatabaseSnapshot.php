<?php

// app/Model/Database.php

class DatabaseSnapshot extends AppModel {
	
	public $useTable = 'db_snapshots';
	
	var $belongsTo = array(
		'Database' => array(
				'className'    => 'Database',
				'foreignKey'    => 'db_id'
		)
	);
	
	public $validate = array(
        'dbsnap_name' => array(
            'rule' => 'notEmpty',
						'message' => 'A database snapshot name is required.'
        ),
				'dbsnap_date' => array(
          'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
        ),
				'dbsnap_data' => array(
            'rule' => 'notEmpty',
						'message' => 'A database password is required.'
        ),
				'db_id' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
				)
    );
	
}

