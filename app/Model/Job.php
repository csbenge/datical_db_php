<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/Model/Job.php
 * 
 */

App::uses('AuthComponent', 'Controller/Component');
App::uses('JobHelper', 'View/Helper');

class Job extends AppModel {
    
  public $validate = array(
  
    'jobname' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'A job name is required.'
        ),
        array(
          'rule' => 'isUnique',
          'message' => 'Job name is already taken.'
        ),
        array(
        'rule' => array('minLength', 4),
        'message' => 'Job name must be at least 4 characters.' 
      ),
    ),
		
		'timepattern', 
		/*
		'timepattern' => array(
			'rule' => 'isValidCronSpec',
			'message' => 'A valid time pattern is required.'
		),
		*/
  
    'command' => array(
			'rule' => 'notEmpty',
			'message' => 'A command to run is required.'
    ),
  
    'notify' => array(
				'rule' => 'notEmpty',
				'message' => 'A command to run is required.'
    ),
  
    'status' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Status is required.',
        'allowEmpty' => false
      )
    ),
  
  );
	
	public function isValidCronSpec($check) {
		$value = array_values($check);
		$value = $value[0];
		
		$jobHelper = new JobHelper(new View());
		$nextrun = $jobHelper->jobNextRun($value);
		if ($nextrun == -1) {
			return 0;
		} else {
			return 1;
		}
	}

}
