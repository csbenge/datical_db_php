<?php

// app/Model/DatabaseServer.php

class DatabaseServer extends AppModel {
	
	public $useTable = 'db_servers';
		
	public $validate = array(
        'dbsvr_name' => array(
            'rule' => 'notEmpty',
						'message' => 'A database server name is required.'
        ),
				'dbsvr_desc' => array(
            'rule' => 'notEmpty',
						'message' => 'A database server description is required.'
        ),
				'dbsvr_type' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'A database server type is required',
						'allowEmpty' => false
						)
				),
				'dbsvr_host' => array(
            'rule' => 'notEmpty',
						'message' => 'A database server host is required.'
        ),
				'dbsvr_port' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'A database server port is required',
						'allowEmpty' => false
					)
				)
    );
	
}

