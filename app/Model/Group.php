<?php

// app/Model/Group.php

App::uses('AuthComponent', 'Controller/Component');

class Group extends AppModel {
	
	 var $belongsTo = array(
		'Role' => array(
				'className'    => 'Role',
				'foreignKey'    => 'role_id'
		)
	);
	 
	
	 var $hasMany = array(
		'GroupMember' => array(
				'className'    => 'GroupMember',
				'foreignKey'    => 'id'
		)
	);
	
    
  public $validate = array(
  
    'groupname' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'A group name is required.'
        ),
        array(
          'rule' => 'isUnique',
          'message' => 'This group is already taken.'
        ),
        array(
        'rule' => array('minLength', 4),
        'message' => 'Group name must be at least 4 characters.' 
      ),
    ),
  
    'description' => array(
				'rule' => 'notEmpty',
				'message' => 'A description is required.'
        )
  
  );
  
}
