<?php

// app/Model/DatabaseRoutes.php

class DatabaseRoute extends AppModel {
	
	public $useTable = 'db_routes';
	
	var $belongsTo = array(
		'DatabaseProject' => array(
				'className'    => 'DatabaseProject',
				'foreignKey'    => 'project_id'
		),
		'Database' => array(
				'className'    => 'Database',
				'foreignKey'    => 'db_id'
		)
	);
	
	public $validate = array(
        'dbroute_step' => array(
            'rule' => 'notEmpty',
						'message' => 'A step is required.'
        ),
				'project_id' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
				),
				'db_id' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
				)
    );
	
}

