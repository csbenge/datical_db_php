<?php

// app/Model/Credential.php

App::uses('AuthComponent', 'Controller/Component');

class Credential extends AppModel {
    
	var $belongsTo = array(
		'DatabaseProject' => array(
				'className'    => 'DatabaseProject',
				'foreignKey'    => 'project_id'
		),
		'User' => array(
				'className'    => 'User',
				'foreignKey'    => 'user_id'
		),
		'Group' => array(
				'className'    => 'Group',
				'foreignKey'    => 'group_id'
		),
		'Role' => array(
				'className'    => 'Role',
				'foreignKey'    => 'role_id'
		)
	);
	
	
		
  public $validate = array(
  
    'project_id' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Need it!',
        'allowEmpty' => false
      )
    ),
  
    'user_id' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Need it!',
        'allowEmpty' => false
      )
    ),
  
    'role_id' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Need it!',
        'allowEmpty' => false
      )
    ),
  
  );
  
}
