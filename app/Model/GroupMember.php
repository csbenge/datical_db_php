<?php

// app/Model/GroupMember.php

App::uses('AuthComponent', 'Controller/Component');

class GroupMember extends AppModel {
  
	/*
	var $hasMany = array(
		'Group' => array(
				'className'    => 'Group',
				'foreignKey'   => 'id'
		)
	);
	*/
	
	var $belongsTo = array(
		'User' => array(
			'className'    => 'User'
		),
		'Group' => array(
			'className'    => 'Group'
		)
		
	);

	public $validate = array(
  
    'group_id' => array(
      'valid' => array(
        'rule' => 'numeric',
        'allowEmpty' => false
				)
      ),
		'user_id' => array(
      'valid' => array(
        'rule' => 'numeric',
        'allowEmpty' => false
      )
		)
	);
	
}
