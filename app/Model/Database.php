<?php

// app/Model/Database.php

class Database extends AppModel {
	
	public $useTable = 'db_databases';
	
	var $belongsTo = array(
		'DatabaseServer' => array(
				'className'    => 'DatabaseServer',
				'foreignKey'    => 'dbsvr_id'
		)
	);
	
	public $validate = array(
        'db_name' => array(
            'rule' => 'notEmpty',
						'message' => 'A database name is required.'
        ),
				'db_desc' => array(
            'rule' => 'notEmpty',
						'message' => 'A database description is required.'
        ),
				'db_env' => array(
            'rule' => 'notEmpty',
						'message' => 'A database environment is required.'
        ),
				'db_username' => array(
            'rule' => 'notEmpty',
						'message' => 'A database user name is required.'
        ),
				'db_password' => array(
            'rule' => 'notEmpty',
						'message' => 'A database password is required.'
        ),
				'dbsvr_id' => array(
					'valid' => array(
						'rule' => 'numeric',
						'message' => 'Need it!',
						'allowEmpty' => false
					)
				)
    );
	
}

