<?php

// app/Model/Role.php

App::uses('AuthComponent', 'Controller/Component');

class Role extends AppModel {
    
  public $validate = array(
  
    'rolename' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'A role name is required.'
        ),
        array(
          'rule' => 'isUnique',
          'message' => 'This role is already taken.'
        ),
        array(
        'rule' => array('minLength', 4),
        'message' => 'Role name must be at least 4 characters.' 
      ),
    ),
  
    'description' => array(
            'rule' => 'notEmpty',
						'message' => 'A description is required.'
        ),
  
    'read' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Allow/Disallow Read.',
        'allowEmpty' => false
      )
    ),
  
    'write' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Allow/Disallow Write.',
        'allowEmpty' => false
      )
    ),
  
    'exec' => array(
      'valid' => array(
        'rule' => 'numeric',
        'message' => 'Allow/Disallow Execute.',
        'allowEmpty' => false
      )
    ),
  
  );
  
}
