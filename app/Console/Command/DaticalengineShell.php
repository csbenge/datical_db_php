<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @copyright     Copyright 2012-2013, Datical, Inc. (http://www.datical.com)
 *                All Rights Reserved
 * @package       datical
 * @subpackage    datical.engine
 * @since         Datical(tm) v 1.1
 *
 * @file: app/Console/Command/Daticalengine.php
 *
 */

require_once(APP . 'Console/Command' . DS . 'tdcron.php');

CakeLog::config('logger', array(
  'engine' => 'FileLog',
  'types' => array('debug', 'info', 'warning', 'error'),
  'scopes' => array('logger'),
  'path' => './',
  'file' => 'datical_engine.log'
));

class DaticalengineShell extends AppShell {
    public $uses = array('User', 'Job');

    public function startUp() {
      CakeLog::info('Starting Datical Engine.', 'logger');
      $this->init_checkFilePerms();
    }
    
    /**
      * main()
      *
      * @package       Datical.Engine.Main
      */
    public function main() {
  
      //$user = $this->User->findByUsername($this->args[0]);
      //$this->out(print_r($user, true));
      
    // while(1) {
       $this->runJobTable();
       sleep(1);
    // }
      
    // $this->poke("localhost", 5000);
    }
 
    public function init_checkFilePerms() {
      CakeLog::info('Checking File Permissions.', 'logger');
      if (($scratch = @tempnam("/tmp","check")) === false || $scratch == '') {
        CakeLog::error('Unable to determine temporary directory', 'logger');
        sleep(3);
        exit();
      }
      if (($f = @fopen($scratch,"w")) === false) {
        CakeLog::error('Unable to write to temporary directory', 'logger'); 
        sleep(3);
        exit();
      }
      fclose($f);
      @unlink($scratch);
    }
  
    public function runJobTable() {
      $time = time();
      $jobs = $this->Job->find('all');
      foreach ($jobs as $job) {
        if ($time > $job['Job']['nextrun']) {         
          // Run job if ready - 'status' values in JobHelper.php
          if ($job['Job']['status'] != 1)
            continue;

          // Set job status to 'Running'
          $this->Job->id = $job['Job']['id'];
          $this->Job->saveField('status',2);


          $logmsg = "Running Job: " . $job['Job']['jobname'];
          CakeLog::debug($logmsg, 'logger');
          $this->runJob($job);

          // Calculate/Update nextrun & set status to 'Ready'
          $jobHelper = new JobHelper(new View());
          $nextrun = $jobHelper->jobNextRun($job['Job']['timepattern']);
          $this->Job->id = $job['Job']['id'];
          $this->Job->saveField('status',1);
          $this->Job->saveField('nextrun',$nextrun);
        }
      }
    }
  
    public function runJob($job) {
      sleep(1);
      // Update Runs Table: Schedule time, Start time, End time, Result code, Output
    }
    
    public function poke($host,$port) {
      CakeLog::info('Poke: Agent: ' . $host . ":" . $port, 'logger');
      if (($socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)) === true) {
        $result = socket_connect($socket,$host,$port);
        if ($result === false) {
          CakeLog::error('Poke: Agent not responding: ' . $host . ":" . $port, 'logger');
        } else {
          CakeLog::info('Poke: Agent Responded: ' . $host . ":" . $port, 'logger');
          //@socket_close($socket);
        }
      } else {
        CakeLog::error('Poke: Cannot create socket: ' . $host . ":" . $port, 'logger');
      }
      
      if (false === ($buf = socket_read($socket, 2048, PHP_NORMAL_READ))) {
        CakeLog::error('Poke: socket_read failed: ' . $host . ":" . $port . '-' . socket_strerror(socket_last_error($socket)), 'logger');
      }
      if (!$buf = trim($buf)) {
        continue;
      }
      if ($buf == 'quit') {
        unset($clients[$key]);
        socket_close($socket);
        break;
      }
      if ($buf == 'shutdown') {
        socket_close($socket);
        break 2;
      }
      
      echo "$buf\n";

    }

}
?>