<?php

// File: app/Controller/GroupMembersController.php

App::import('Controller', 'Groups');

class GroupMembersController extends AppController {
  public $helpers = array('Button', 'Role');

  public function view($id = null) {
    $this->GroupMember->id = $id;
    if (!$this->GroupMember->exists()) {
        throw new NotFoundException(__('Invalid group member.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('groupmembers', $this->Group->read(null, $id));
  }

  public function add() {
  
    $this->loadModel('User');
    $data = $this->User->find('all', array('fields' => 'User.id,  User.username')); 
    $user_list = Set::combine($data, '{n}.User.id', '{n}.User.username'); 
    $this->set('user_list', $user_list);
    
    $this->loadModel('Group');
    $data = $this->Group->find('all', array('fields' => 'Group.id,  Group.groupname')); 
    $group_list = Set::combine($data, '{n}.Group.id', '{n}.Group.groupname'); 
    $this->set('group_list', $group_list);
  
    if ($this->request->is('post')) {
      $this->GroupMember->create();
      if ($this->GroupMember->save($this->request->data)) {
          $this->Session->setFlash(__('Member has been added to group.', 'default', array('class' => 'alert alert-success')));
          //$this->redirect(array('action' => 'index'));
          $group_id = $this->request->data['GroupMember']['group_id'];
          $this->redirect(array('controller' => 'Groups', 'action' => 'view', $group_id));
      } else {
          $this->Session->setFlash(__('Member could not be saved. Please, try again.'));
      }
    }
  }

  public function delete($id = null, $group_id) {

    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->GroupMember->id = $id;
    if (!$this->GroupMember->exists()) {
      throw new NotFoundException(__('Invalid group member.', 'default', array('class' => 'alert alert-error')));
    }
    
    if ($this->GroupMember->delete()) {
      $this->Session->setFlash(__('Member has been removed.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('controller' => 'Groups', 'action' => 'view', $group_id));
    }
    $this->Session->setFlash(__('Member was not removed', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('controller' => 'Groups', 'action' => 'view', $group_id));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'User.username' => 'asc'
      )
  );

}