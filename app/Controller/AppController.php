<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file.
 * 
 * @copyright     Copyright (c) Datical, Inc.
 * @link          hhttp://www.datical.com
 * @package       app.Controller
 * @since         Datical(tm) v 1.2.9
 * @license       ?
 */

App::uses('Controller', 'Controller');
App::uses('AppHelper', 'View/Helper');

class AppController extends Controller {
  public $helpers = array('Html', 'Form', 'Session', 'Js');
     
  public $components = array(
    'Session',
    'Auth' => array(
        'loginRedirect' => array('controller' => 'databaseprojects', 'action' => 'index'),
        'logoutRedirect' => array('controller' => 'users', 'action' => 'login')
      )
    );

  function beforeFilter() {
    $this->Auth->allow('login');
    $this->Session->delete('Message.auth');
    $this->set('username', $this->_usersUsername());
    $this->set('userrole', $this->_usersUserrole());
    
    setlocale(LC_ALL, "ru_RU.utf8");
    Configure::write('Config.language', 'eng');
    CakeSession::write('Config.language', 'eng');
  }
  
  function _usersUsername(){
    $users_username = NULL;
    if($this->Auth->user()){
      $users_username = $this->Auth->user('username');
    }
    return $users_username;
  }
  
  function _usersUserrole(){
    $users_userrole = NULL;
    if($this->Auth->user()){
      
      $this->loadModel('User');
      $users_userrole = $this->User->find('first', array(
        'conditions' => array('User.role_id' => $this->Auth->user('role_id'))));
    }
    return $users_userrole['Role']['rolename'];
  }
  
}
