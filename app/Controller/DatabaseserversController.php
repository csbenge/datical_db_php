<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/Controller/DatabaseserversController.php
 * 
 */

App::uses('Datical', 'View/Helper');

class DatabaseServersController extends AppController {
  public $helpers = array('Button', 'Text', 'Datical');

  public function index() {
    $this->DatabaseServer->recursive = 0;
    $this->set('databaseservers', $this->paginate());
  }

  public function view($id = null) {
    $this->DatabaseServer->id = $id;
    if (!$this->DatabaseServer->exists()) {
        throw new NotFoundException(__('Invalid database server.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('databaseserver', $this->DatabaseServer->read(null, $id));
  }

  public function add() {
    if ($this->request->is('post')) {
      $this->DatabaseServer->create();      
      if ($this->DatabaseServer->save($this->request->data)) {
          $this->Session->setFlash(__('The database server has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The database server could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    $this->DatabaseServer->id = $id;
    if (!$this->DatabaseServer->exists()) {
      throw new NotFoundException(__('Invalid database server', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->DatabaseServer->save($this->request->data)) {
        $this->Session->setFlash(__('The database server has been updated.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The database server could not be updated. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->DatabaseServer->read(null, $id);
    }
  }

  public function delete($id = null) {   
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->DatabaseServer->id = $id;
    if (!$this->DatabaseServer->exists()) {
      throw new NotFoundException(__('Invalid database server.', 'default', array('class' => 'alert alert-error')));
    }
    
    // Should not delete a server as has databases linked
    $this->loadModel('Database');
    $db_assigned = $this->Database->find('first', array(
        'conditions' => array('Database.dbsvr_id' => $id)));
    if ($db_assigned) {
      $this->Session->setFlash(__('Cannot delete a database server that has databases assigned.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }

    if ($this->DatabaseServer->delete()) {
      $this->Session->setFlash(__('Database server deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Database server was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'DatabaseServer.dbsvr_name' => 'asc'
      )
  );

}