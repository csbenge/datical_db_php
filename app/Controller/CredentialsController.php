<?php

// File: app/Controller/CredentialsController.php

App::import('Controller', 'DatabaseProjects');
App::import('Controller', 'Users');
App::import('Controller', 'Roles');

class CredentialsController extends AppController {
  public $helpers = array('Button', 'Role');

  public function index() {
    $this->Credential->recursive = 0;  
    $this->set('credentials', $this->paginate());
  }

  public function view($id = null) {
    $this->Credential->id = $id;
    if (!$this->Credential->exists()) {
        throw new NotFoundException(__('Invalid credential.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('credential', $this->Credential->read(null, $id));
  }

  public function add() {
    
    $this->loadModel('DatabaseProject');
    $data = $this->DatabaseProject->find('all', array('fields' => 'DatabaseProject.id,  DatabaseProject.projectname')); 
    $project_list = Set::combine($data, '{n}.DatabaseProject.id', '{n}.DatabaseProject.projectname'); 
    $this->set('project_list', $project_list);
    
    $this->loadModel('User');
    $data = $this->User->find('all', array('fields' => 'User.id,  User.username')); 
    $user_list = Set::combine($data, '{n}.User.id', '{n}.User.username'); 
    $this->set('user_list', $user_list);
    
    $this->loadModel('Group');
    $data = $this->Group->find('all', array('fields' => 'Group.id,  Group.groupname')); 
    $group_list = Set::combine($data, '{n}.Group.id', '{n}.Group.groupname'); 
    $this->set('group_list', $group_list);
    
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list);
    
    if ($this->request->is('post')) {
      $this->Credential->create();
      if ($this->Credential->save($this->request->data)) {
          $this->Session->setFlash(__('The credential has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The credential could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    
    $this->loadModel('DatabaseProject');
    $data = $this->DatabaseProject->find('all', array('fields' => 'DatabaseProject.id,  DatabaseProject.projectname')); 
    $project_list = Set::combine($data, '{n}.DatabaseProject.id', '{n}.DatabaseProject.projectname'); 
    $this->set('project_list', $project_list);
    
    $this->loadModel('User');
    $data = $this->User->find('all', array('fields' => 'User.id,  User.username')); 
    $user_list = Set::combine($data, '{n}.User.id', '{n}.User.username'); 
    $this->set('user_list', $user_list);
    
    $this->loadModel('Group');
    $data = $this->Group->find('all', array('fields' => 'Group.id,  Group.groupname')); 
    $group_list = Set::combine($data, '{n}.Group.id', '{n}.Group.groupname'); 
    $this->set('group_list', $group_list);
    
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list);
    
    $this->Credential->id = $id;
    if (!$this->Credential->exists()) {
      throw new NotFoundException(__('Invalid credential', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Credential->save($this->request->data)) {
        $this->Session->setFlash(__('The credential has been saved.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The credential could not be saved. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->Credential->read(null, $id);
    }
  }

  public function delete($id = null) {
    
    //
    // Should not be able to delete a ole if assigned to user(s)
    //
    
    $this->loadModel('User');
    $user_assigned = $this->User->find('first', array(
        'conditions' => array('User.credential_id' => $id)));
    if ($user_assigned) {
      $this->Session->setFlash(__('Cannot delete a credential that has users.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Credential->id = $id;
    if (!$this->Credential->exists()) {
      throw new NotFoundException(__('Invalid credential.', 'default', array('class' => 'alert alert-error')));
    }
    
    $credentialname = $this->User->find('first', array(
        'conditions' => array('Credential.id' => $id)));
    if ($credentialname['Credential']['credentialname'] == "Admin") {
      $this->Session->setFlash(__('Cannot delete Admin credential.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    if ($this->Credential->delete()) {
      $this->Session->setFlash(__('Credential deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Credential was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'Credential.credentialname' => 'asc'
      )
  );

}