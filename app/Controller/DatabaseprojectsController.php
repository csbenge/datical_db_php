<?php

// app/Controller/DatabaseProjectsController.php

App::import('Controller', 'Databases');
App::import('Controller', 'DatabaseRoutes');

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Xml', 'Utility');

class DatabaseProjectsController extends AppController {
	public $components = array('Session');
	public $helpers = array('Database');
	
/*
 *----------------------------------
 * BEGIN: Datical DB Engine Calls
 * ---------------------------------
 */
	
	public function connect() {
		return 2;
	}
	
	public function snapshot() {
		// take snapshot
		// stuff in database
	}
	
	public function compare() {
	}
	
	public function forecast() {
	}
	
	public function deploy() {
	}

	public function rollback() {
	}
	
	public function history() {
	}
	
/*
 *----------------------------------
 * END: Datical DB Engine Calls
 * ---------------------------------
 */
	
	public function index() {
		$this->DatabaseProject->recursive = 0;
    $this->set('projects', $this->paginate());
	}
	
	// View

	public function view($id) {
		if (!$id) {
			throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}

		$project = $this->DatabaseProject->findById($id);
		if (!$project) {
			throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}

		// Get the database route and related reports and snapshots for this project 
		$this->loadModel('Database');
		$this->loadModel('DatabaseRoute');
		$db_routes = $this->DatabaseRoute->find('all', array('fields' => 'DatabaseRoute.id, DatabaseRoute.db_id',
																									 'conditions' => array('DatabaseRoute.project_id' => $id),
																									 'order' => array('DatabaseRoute.dbroute_step' => 'asc'))); 
		$i=0;
    foreach ($db_routes as $db_route) {
			$route = $this->Database->find('first', array('fields' => 'Database.id, Database.db_name',
																				'conditions' => array('Database.id' => $db_route['DatabaseRoute']['db_id'])));
			$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.db_name", $route['Database']['db_name']);
			$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.db_status", $this->connect());
			//$db_route_list[$i][0][0] = $data['Database']['db_name'];
			//$db_route_list[$i][0][1] = $data['Database']['id'];
			//$db_route_list[$i][0][2] = $d['DatabaseRoute']['id'];
			
				// Reports for this Route
				$tmp = new Folder(WWW_ROOT);
				$this->loadModel('DatabaseReport');
				$db_reports = $this->DatabaseReport->find('all', array('fields' => 'DatabaseReport.id, DatabaseReport.dbrep_name, DatabaseReport.dbrep_type, DatabaseReport.db_id',
																											'conditions' => array('DatabaseReport.db_id' => $route['Database']['id']),
																											'order' => array('DatabaseReport.dbrep_date' => 'asc')));
				$j=0;
				$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseReport", array());
				foreach ($db_reports as $db_report) {
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseReport.$j.id", $db_report['DatabaseReport']['id']);
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseReport.$j.dbrep_name", $db_report['DatabaseReport']['dbrep_name']);
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseReport.$j.dbrep_file", 'ff');
					$j++;
				}
			
				// Snapshots of this Route
				$tmp = new Folder(WWW_ROOT);
				$this->loadModel('DatabaseSnapshot');
				$db_snapshots = $this->DatabaseSnapshot->find('all', array('fields' => 'DatabaseSnapshot.id, DatabaseSnapshot.dbsnap_name, DatabaseSnapshot.db_id',
																											'conditions' => array('DatabaseSnapshot.db_id' => $route['Database']['id']),
																											'order' => array('DatabaseSnapshot.dbsnap_date' => 'asc')));
				$j=0;
				$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseSnapshot", array());
				foreach ($db_snapshots as $db_snapshot) {
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseSnapshot.$j.id", $db_snapshot['DatabaseSnapshot']['id']);
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseSnapshot.$j.dbsnap_name", $db_snapshot['DatabaseSnapshot']['dbsnap_name']);
					$db_routes = Set::insert($db_routes, "$i.DatabaseRoute.Database.DatabaseSnapshot.$j.dbsnap_file", 'ff');
					$j++;
				}

			$i++;
		}
    $this->set('db_routes', $db_routes);
		
		// Process Reports
		$db_wksp = '/files/Vehicles';
		$dir = new Folder(WWW_ROOT . $db_wksp . '/Reports/');
		$filelist = $dir->read(true);
		$this->set('rp_filelist', $filelist);
		$this->set('rp_filedir', $db_wksp . '/Reports/');
		
		// Process Snapshots
		/*
		$db_wksp = '/files/Vehicles';
		$dir = new Folder(WWW_ROOT . $db_wksp . '/Snapshots/');
		$filelist = $dir->find('.*\.xml', true);
		$this->set('sn_filelist', $filelist);
		$this->set('sn_filedir', $db_wksp . '/Snapshots/');
		*/

		$this->set('project', $project);
	}
	
	public function view_snapshots($id) {
		if (!$id) {
			throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}
	}
	
	// Add

	public function add() {
    if ($this->request->is('post')) {
      $this->DatabaseProject->create();
      if ($this->DatabaseProject->save($this->request->data)) {
          $this->Session->setFlash(__('Project has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The project not be saved. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    }
  }
	
	public function add_step($id) {
		
		// TODO: the list generated below should only have unassigned databases
		$this->loadModel('Database');
    $data = $this->Database->find('all', array('fields' => 'Database.id,  Database.db_name')); 
    $database_list = Set::combine($data, '{n}.Database.id', '{n}.Database.db_name'); 
    $this->set('database_list', $database_list);

		$project = $this->DatabaseProject->findById($id);
		if (!$project) {
			throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}
		$this->set('project', $project);

		if ($this->request->is('post')) {
			$this->loadModel('DatabaseRoute');
			$this->DatabaseRoute->saveField('project_id',$id);

			if ($this->DatabaseRoute->save($this->request->data)) {
						$this->Session->setFlash(__('Step has been added.', 'default', array('class' => 'alert alert-success')));
						$this->redirect(array('action' => 'index'));
				} else {
						$this->Session->setFlash(__('The step could not be added. Please, try again.', 'default', array('class' => 'alert alert-error')));
			}
		}
  }
	
	// Edit

	public function edit($id = null) {
		if (!$id) {
		  throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}

		$project = $this->DatabaseProject->findById($id);
		if (!$project) {
		    throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
		    $this->DatabaseProject->id = $id;
		    if ($this->DatabaseProject->save($this->request->data)) {
		        $this->Session->setFlash(__('Project has been updated.'), 'default', array('class' => 'alert alert-success'));
		        $this->redirect(array('action' => 'index'));
		    } else {
		        $this->Session->setFlash(__('Unable to update your project.', 'default', array('class' => 'alert alert-error')));
		    }
		}

		if (!$this->request->data) {
		    $this->request->data = $project;
		}
	}
	
	// Delete

	public function delete($id = null) {
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->DatabaseProject->id = $id;
    if (!$this->DatabaseProject->exists()) {
      throw new NotFoundException(__('Invalid project', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->DatabaseProject->delete()) {
      $this->Session->setFlash(__('Project deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Project was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }
	
	public function delete_report($id = null) {
		$this->loadModel('DatabaseReport');
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->DatabaseReport->id = $id;
    if (!$this->DatabaseReport->exists()) {
      throw new NotFoundException(__('Invalid report', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->DatabaseReport->delete()) {
      $this->Session->setFlash(__('Report deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Report was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }
	
	public function delete_snapshot($id = null) {
		$this->loadModel('DatabaseSnapshot');
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->DatabaseSnapshot->id = $id;
    if (!$this->DatabaseSnapshot->exists()) {
      throw new NotFoundException(__('Invalid snapshot', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->DatabaseSnapshot->delete()) {
      $this->Session->setFlash(__('Snapshot deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Snapshot was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }
	
	public function delete_step($id = null) {
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
		
		$this->loadModel('DatabaseRoute');
    $this->DatabaseRoute->id = $id;
    if (!$this->DatabaseRoute->exists()) {
      throw new NotFoundException(__('Invalid database route', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->DatabaseRoute->delete()) {
      $this->Session->setFlash(__('Database step deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Database step  was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }
	
	// Paginate
	
	public $paginate = array(
      'limit' => 10,
      'order' => array(
          'DatabaseProject.projectname' => 'asc'
      )
  );
	
}
