<?php

// File: app/Controller/UsersController.php

App::import('Controller', 'Roles');

class UsersController extends AppController {
  public $helpers = array('Button', 'Role');
   
  public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout');
  }
    
  public function login() {
    if ($this->Auth->login()) {
      $logmsg = "User " . $this->Auth->user('username') . " " . __('logged in.');
      CakeLog::write('debug', $logmsg);
      //$this->Session->setFlash('Login Successful!', 'default', array('class' => 'alert alert-success'));
      $this->redirect($this->Auth->redirect());
    } else if ($this->Auth->user('username') != "") {
      $this->Session->setFlash('Invalid username or password, try again', 'default', array('class' => 'alert alert-error'));
    }
  }

  public function logout() {
    $this->Session->setFlash('Logout Successful!', 'default', array('class' => 'alert alert-success'));
    $logmsg = "User " . $this->Auth->user('username')  . " " . __('logged out.');
    CakeLog::write('debug', $logmsg);
    $this->Session->destroy();
    $this->redirect($this->Auth->logout());
  }

  public function index() {
    $this->User->recursive = 0;
    $this->set('users', $this->paginate());
  }

  public function view($id = null) {
    $this->User->id = $id;
    if (!$this->User->exists()) {
        throw new NotFoundException(__('Invalid user', 'default', array('class' => 'alert alert-error')));
    }
    
    $this->set('user', $this->User->read(null, $id));
    
    // Get groups assigned to use
    
    $this->loadModel('GroupMember');
    $data = $this->GroupMember->find('all', array('fields' => 'GroupMember.id,  GroupMember.group_id', 'conditions' => array('GroupMember.user_id' => $id)));

    // Get names of groups assigned
    
    $this->loadModel('Group');
    $groups = "";
    foreach ($data as $d) {
      $groupname = $this->Group->find('first', array('fields' => 'Group.groupname',
        'conditions' => array('Group.id' => $d['GroupMember']['group_id'])));
      $groups = $groups . $groupname['Group']['groupname'] . ', ';
    }
    if ($groups == "")
      $groups = "None";
    
    $this->set('groups', $groups);
  }

  public function add() {
    
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list); 
    
    if ($this->request->is('post')) {
      $this->User->create();
      if ($this->User->save($this->request->data)) {
          $this->Session->setFlash(__('The user has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list);  
    
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->User->save($this->request->data)) {
        $this->Session->setFlash(__('The user has been saved.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The user could not be saved. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->User->read(null, $id);
      unset($this->request->data['User']['password']);
    }
  }

  public function delete($id = null) {
    
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user', 'default', array('class' => 'alert alert-error')));
    }
    
    // Cannot delete root
    
    $username = $this->User->find('first', array(
        'conditions' => array('User.id' => $id)));
    if ($username['User']['username'] == "root") {
      $this->Session->setFlash(__('Cannot delete root user.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    // Should not be able to delete a user if has credentials(s)
    
    $this->loadModel('Credential');
    $cred_assigned = $this->Credential->find('first', array(
        'conditions' => array('Credential.user_id' => $id)));
    if ($cred_assigned) {
      $this->Session->setFlash(__('Cannot delete a user that has credentials.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    if ($this->User->delete()) {
      $this->Session->setFlash(__('User deleted', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('User was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }
  
  
  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'User.username' => 'asc'
      )
  );

}