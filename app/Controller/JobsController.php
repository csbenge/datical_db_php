<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/Controller/JobsController.php
 * 
 */

App::uses('JobHelper', 'View/Helper');

class JobsController extends AppController {
  public $helpers = array('Button', 'Text', 'Job');

  public function index() {
    $this->Job->recursive = 0;
    $this->set('jobs', $this->paginate());
  }

  public function view($id = null) {
    $this->Job->id = $id;
    if (!$this->Job->exists()) {
        throw new NotFoundException(__('Invalid job.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('job', $this->Job->read(null, $id));
  }

  public function add() {
    if ($this->request->is('post')) {
      $this->Job->create();
      
      $jobHelper = new JobHelper(new View());
      // $timepattern = $this->request->data['Job']['timepattern'];
      $timepattern =  $this->request->data['Job']['iminute'] . ' ' .
                      $this->request->data['Job']['ihour'] . ' ' .
                      $this->request->data['Job']['iday'] . ' ' .
                      $this->request->data['Job']['imonth'] . ' ' .
                      $this->request->data['Job']['iweekday'] . ' ';  
      $nextrun = $jobHelper->jobNextRun($timepattern);
      if ($nextrun == -1) {
        $this->Session->setFlash(__('Time pattern is not valid.'));
        $this->redirect(array('action' => 'add'));
      }
      $this->request->data['Job']['timepattern'] = $timepattern;
      $this->Job->saveField('nextrun',$nextrun);
      
      if ($this->Job->save($this->request->data)) {
          $this->Session->setFlash(__('The job has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The job could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    $this->Job->id = $id;
    if (!$this->Job->exists()) {
      throw new NotFoundException(__('Invalid job', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      $jobHelper = new JobHelper(new View());
      $timepattern =  $this->request->data['Job']['iminute'] . ' ' .
                      $this->request->data['Job']['ihour'] . ' ' .
                      $this->request->data['Job']['iday'] . ' ' .
                      $this->request->data['Job']['imonth'] . ' ' .
                      $this->request->data['Job']['iweekday'] . ' ';
      $nextrun = $jobHelper->jobNextRun($timepattern);
      if ($nextrun == -1) {
        $this->Session->setFlash(__('Time pattern is not valid.'));
        $this->redirect(array('action' => 'add'));
      }
      $this->request->data['Job']['timepattern'] = $timepattern;
      $this->Job->saveField('nextrun',$nextrun);
  
      if ($this->Job->save($this->request->data)) {
        $this->Session->setFlash(__('The job has been updated.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The job could not be updated. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->Job->read(null, $id);
    }
  }

  public function delete($id = null) {   
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Job->id = $id;
    if (!$this->Job->exists()) {
      throw new NotFoundException(__('Invalid job.', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->Job->delete()) {
      $this->Session->setFlash(__('Job deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Job was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'Job.jobname' => 'asc'
      )
  );

}