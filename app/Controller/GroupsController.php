<?php

// File: app/Controller/GroupsController.php

App::import('Controller', 'Roles');
App::import('Controller', 'GroupMembers');

class GroupsController extends AppController {
  public $helpers = array('Button', 'Role');

  public function index() {
    $this->Group->recursive = 0;
    $this->set('groups', $this->paginate());
  }

  public function view($id = null) {
    $this->Group->id = $id;
    if (!$this->Group->exists()) {
        throw new NotFoundException(__('Invalid group.', 'default', array('class' => 'alert alert-error')));
    }
  
    $this->set('group', $this->Group->read(null, $id));
  
    $this->loadModel('GroupMember');
    $member_list = $this->GroupMember->find('all', array('conditions' => array('Group.id' => $id),
                                                         'order' => array('User.username ASC')));
    
    $group_id = $id;
    $this->loadModel('GroupMember');
    $member_count = $this->GroupMember->find('count', array('conditions'=>array('GroupMember.group_id' => $group_id)));
    $this->set('totalmembers', $member_count);

    $this->set('groupmembers', $member_list);
  }

  public function add() {
  
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list);
  
    if ($this->request->is('post')) {
      $this->Group->create();
      if ($this->Group->save($this->request->data)) {
          $this->Session->setFlash(__('Group has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('Group could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    
    $this->loadModel('Role');
    $data = $this->Role->find('all', array('fields' => 'Role.id,  Role.rolename')); 
    $role_list = Set::combine($data, '{n}.Role.id', '{n}.Role.rolename'); 
    $this->set('role_list', $role_list);
    
    $this->Group->id = $id;
    if (!$this->Group->exists()) {
      throw new NotFoundException(__('Invalid group', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Group->save($this->request->data)) {
        $this->Session->setFlash(__('The group has been saved.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The group could not be saved. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->Group->read(null, $id);
    }
  }

  public function delete($id = null) {
  
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Group->id = $id;
    if (!$this->Group->exists()) {
      throw new NotFoundException(__('Invalid group.', 'default', array('class' => 'alert alert-error')));
    }
  
    // Cannot delete group with members
  
    $this->loadModel('GroupMember');
    $has_members = $this->GroupMember->find('first', array(
        'conditions' => array('GroupMember.group_id' => $id)));
    if ($has_members) {
      $this->Session->setFlash(__('Cannot delete a group that has members.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    } 
  
    if ($this->Group->delete()) {
      $this->Session->setFlash(__('Group deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Group was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'Group.groupname' => 'asc'
      )
  );

}