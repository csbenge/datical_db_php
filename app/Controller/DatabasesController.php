<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/Controller/DatabasesController.php
 * 
 */

App::import('Controller', 'DatabaseServers');
 
App::uses('Datical', 'View/Helper');

class DatabasesController extends AppController {
  public $helpers = array('Button', 'Text', 'Datical');

  public function index() {
    $this->Database->recursive = 0;
    $this->set('databases', $this->paginate());
  }

  public function view($id = null) {
    $this->Database->id = $id;
    if (!$this->Database->exists()) {
        throw new NotFoundException(__('Invalid databases.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('database', $this->Database->read(null, $id));
  }

  public function add() {
    
    $this->loadModel('DatabaseServer');
    $data = $this->DatabaseServer->find('all', array('fields' => 'DatabaseServer.id,  DatabaseServer.dbsvr_name')); 
    $server_list = Set::combine($data, '{n}.DatabaseServer.id', '{n}.DatabaseServer.dbsvr_name'); 
    $this->set('server_list', $server_list);
    
    if ($this->request->is('post')) {
      $this->Database->create();      
      if ($this->Database->save($this->request->data)) {
          $this->Session->setFlash(__('The databases has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The databases could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    
    $this->loadModel('DatabaseServer');
    $data = $this->DatabaseServer->find('all', array('fields' => 'DatabaseServer.id,  DatabaseServer.dbsvr_name')); 
    $server_list = Set::combine($data, '{n}.DatabaseServer.id', '{n}.DatabaseServer.dbsvr_name'); 
    $this->set('server_list', $server_list);
    
    $this->Database->id = $id;
    if (!$this->Database->exists()) {
      throw new NotFoundException(__('Invalid databases', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Database->save($this->request->data)) {
        $this->Session->setFlash(__('The databases has been updated.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The databases could not be updated. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->Database->read(null, $id);
    }
  }

  public function delete($id = null) {   
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Database->id = $id;
    if (!$this->Database->exists()) {
      throw new NotFoundException(__('Invalid databases.', 'default', array('class' => 'alert alert-error')));
    }
    
    // Should not delete a database that is tied to a deployment plan
    $this->loadModel('DatabaseRoute');
    $db_assigned = $this->DatabaseRoute->find('first', array(
        'conditions' => array('DatabaseRoute.db_id' => $id)));
    if ($db_assigned) {
      $this->Session->setFlash(__('Cannot delete a database server that is assigned to a deployment plan.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }

    if ($this->Database->delete()) {
      $this->Session->setFlash(__('Database deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Database was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'Database.databasesname' => 'asc'
      )
  );

}