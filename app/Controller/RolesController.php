<?php

// File: app/Controller/RolesController.php

class RolesController extends AppController {
  public $helpers = array('Button', 'Role');

  public function index() {
    $this->Role->recursive = 0;
    $this->set('roles', $this->paginate());
  }

  public function view($id = null) {
    $this->Role->id = $id;
    if (!$this->Role->exists()) {
        throw new NotFoundException(__('Invalid role.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('role', $this->Role->read(null, $id));
  }

  public function add() {
    if ($this->request->is('post')) {
      $this->Role->create();
      if ($this->Role->save($this->request->data)) {
          $this->Session->setFlash(__('The role has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The role could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    $this->Role->id = $id;
    if (!$this->Role->exists()) {
      throw new NotFoundException(__('Invalid role', 'default', array('class' => 'alert alert-error')));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Role->save($this->request->data)) {
        $this->Session->setFlash(__('The role has been saved.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The role could not be saved. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->Role->read(null, $id);
    }
  }

  public function delete($id = null) {
    
    //
    // Should not be able to delete a role if assigned to user(s)
    //
    
    $this->loadModel('User');
    $user_assigned = $this->User->find('first', array(
        'conditions' => array('User.role_id' => $id)));
    if ($user_assigned) {
      $this->Session->setFlash(__('Cannot delete a role that has users.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Role->id = $id;
    if (!$this->Role->exists()) {
      throw new NotFoundException(__('Invalid role.', 'default', array('class' => 'alert alert-error')));
    }
    
    $rolename = $this->User->find('first', array(
        'conditions' => array('Role.id' => $id)));
    if ($rolename['Role']['rolename'] == "Admin") {
      $this->Session->setFlash(__('Cannot delete Admin role.', 'default', array('class' => 'alert alert-error')));
      $this->redirect(array('action' => 'index'));
    }
    
    if ($this->Role->delete()) {
      $this->Session->setFlash(__('Role deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Role was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'Role.rolename' => 'asc'
      )
  );

}