<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/Controller/DatabaseRoutesController.php
 * 
 */

App::uses('Datical', 'View/Helper');

class DatabaseRoutesController extends AppController {
  public $helpers = array('Button', 'Text', 'Datical');

  public function index() {
    $this->DB_Route->recursive = 0;
    $this->set('db_routes', $this->paginate());
  }

  public function view($id = null) {
    $this->DB_Route->id = $id;
    if (!$this->DB_Route->exists()) {
        throw new NotFoundException(__('Invalid db_route.', 'default', array('class' => 'alert alert-error')));
    }
    $this->set('db_route', $this->DB_Route->read(null, $id));
  }

  public function add() {
    if ($this->request->is('post')) {
      $this->DB_Route->create();      
      if ($this->DB_Route->save($this->request->data)) {
          $this->Session->setFlash(__('The db_route has been saved.', 'default', array('class' => 'alert alert-success')));
          $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The db_route could not be saved. Please, try again.'));
      }
    }
  }

  public function edit($id = null) {
    $this->DB_Route->id = $id;
    if (!$this->DB_Route->exists()) {
      throw new NotFoundException(__('Invalid db_route', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->DB_Route->save($this->request->data)) {
        $this->Session->setFlash(__('The db_route has been updated.', 'default', array('class' => 'alert alert-success')));
        $this->redirect(array('action' => 'index'));
      } else {
          $this->Session->setFlash(__('The db_route could not be updated. Please, try again.', 'default', array('class' => 'alert alert-error')));
      }
    } else {
      $this->request->data = $this->DB_Route->read(null, $id);
    }
  }

  public function delete($id = null) {   
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->DB_Route->id = $id;
    if (!$this->DB_Route->exists()) {
      throw new NotFoundException(__('Invalid db_route.', 'default', array('class' => 'alert alert-error')));
    }

    if ($this->DB_Route->delete()) {
      $this->Session->setFlash(__('DB_Route deleted.', 'default', array('class' => 'alert alert-success')));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('DB_Route was not deleted', 'default', array('class' => 'alert alert-error')));
    $this->redirect(array('action' => 'index'));
  }

  public $paginate = array(
      'limit' => 10,
      'order' => array(
          'DB_Route.db_routename' => 'asc'
      )
  );

}