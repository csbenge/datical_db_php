<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Jobs/index.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/jobs');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>

<div class="container-fluid center">
  <div class="jobs index">
  
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Jobs'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Job')); ?></p>
      </div>
  
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
       <!--   <th style="background-color: #e5e5e5"><?php echo $this->Paginator->sort('id');?></th> -->
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('jobname', __('Job Name'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('timepattern', __('Time Pattern'));?></th>
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('nextrun', __('Next Run'));?></th>
          <th style="background-color: #e5e5e5"><?php echo $this->Paginator->sort('command', __('Command'));?></th>
          <th style="background-color: #e5e5e5;width: 125px"><?php echo $this->Paginator->sort('notify', __('Notify'));?></th>
          <th style="text-align: center;background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('status', __('Status'));?></th>
          <th class="actions" style="text-align: center;background-color: #e5e5e5;width: 100px"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($jobs as $job): ?>
          <tr>
         <!--   <td><?php echo h($job['Job']['id']); ?></td> -->
            <td><?php echo $this->Html->link($job['Job']['jobname'], array('action' => 'view', $job['Job']['id'])); ?></td>
            <td><?php echo h($job['Job']['timepattern']); ?></td>
            <td><?php echo $this->Job->jobNextRunPrint($job['Job']['nextrun']); ?></td>
            <td><?php echo h($job['Job']['command']); ?></td>
            <td><?php echo $this->Job->jobNotifyLabel($job['Job']['notify']); ?></td>
            <td style="text-align: center;"><?php echo $this->Job->jobStatusLabel($job['Job']['status']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($job['Job']['id']); ?>
              <?php echo $this->Button->deleteButton($job['Job']['id'], $job['Job']['jobname']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>

    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} jobs out of {:count} total, starting on job {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
