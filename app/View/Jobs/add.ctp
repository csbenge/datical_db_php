<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Jobs/add.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/jobs');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
  
<div class="container-fluid center">
  <div class="jobs form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Job'); ?>
          <fieldset>
            <legend><b><?php echo __('Add Job'); ?></b></legend>

          <div class="container">

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Job Name')); ?></div>
               <div class="span10"><?php echo $this->Form->input('jobname', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Minute')); ?></div>
               <div class="span10"><?php echo $this->Form->input('iminute', array(
                    'label' => false,
                    'type' => 'select',
                    'style'=>'width:75px;',
                    'options' => array(
                        '*' => __('Every'),
                        '*/5' => '*/5',
                        '*/10' => '*/10',
                        '*/15' => '*/15',
                        '*/30' => '*/30'
                       
                      )
                    )
                  ); ?>
               </div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Hour')); ?></div>
               <div class="span10"><?php echo $this->Form->input('ihour', array(
                    'label' => false,
                    'type' => 'select',
                    'style'=>'width:75px;',
                    'options' => array(
                        '*' => __('Every'),
                        '0' => '0',
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                        '9' => '9',
                        '10' => '10',
                        '11' => '11',
                        '12' => '12',
                        '13' => '13',
                        '14' => '14',
                        '15' => '15',
                        '16' => '16',
                        '17' => '17',
                        '18' => '18',
                        '19' => '19',
                        '20' => '20',
                        '21' => '21',
                        '22' => '22',
                        '23' => '23'
                      )
                    )
                  ); ?>
               </div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Day')); ?></div>
               <div class="span10"><?php echo $this->Form->input('iday', array(
                    'label' => false,
                    'type' => 'select',
                    'style'=>'width:75px;',
                    'options' => array(
                        '*' => __('Every'),
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                        '9' => '9',
                        '10' => '10',
                        '11' => '11',
                        '12' => '12',
                        '13' => '13',
                        '14' => '14',
                        '15' => '15',
                        '16' => '16',
                        '17' => '17',
                        '18' => '18',
                        '19' => '19',
                        '20' => '20',
                        '21' => '21',
                        '22' => '22',
                        '23' => '23',
                        '24' => '24',
                        '25' => '25',
                        '26' => '26',
                        '27' => '27',
                        '28' => '28',
                        '29' => '29',
                        '30' => '30',
                        '31' => '31'
                      )
                    )
                  ); ?>
               </div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Month')); ?></div>
               <div class="span10"><?php echo $this->Form->input('imonth', array(
                    'label' => false,
                    'type' => 'select',
                    'style'=>'width:120px;',
                    'options' => array(
                        '*' => __('Every'),
                        '1' => __('January'),
                        '2' => __('February'),
                        '3' => __('March'),
                        '4' => __('April'),
                        '5' => __('May'),
                        '6' => __('June'),
                        '7' => __('July'),
                        '8' => __('August'),
                        '9' => __('September'),
                        '10' => __('October'),
                        '11' => __('November'),
                        '12' => __('December')
                      )
                    )
                  ); ?>
               </div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Week Day')); ?></div>
               <div class="span10"><?php echo $this->Form->input('iweekday', array(
                    'label' => false,
                    'type' => 'select',
                    'style'=>'width:120px;',
                    'options' => array(
                        '*' => __('Every'),
                        '1' => __('Sunday'),
                        '2' => __('Monday'),
                        '3' => __('Tuesday'),
                        '4' => __('Wednesday'),
                        '5' => __('Thursday'),
                        '6' => __('Friday'),
                        '7' => __('Saturday')
                      )
                    )
                  ); ?>
               </div>
             </div>

             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Command')); ?></div>
               <div class="span10"><?php echo $this->Form->input('command', array('label' => false)); ?></div>
             </div>

            <div class="row">
              <div class="span2"><?php echo $this->Html->tag('b', __('Notify')); ?></div>
              <div class="span10"><?php echo $this->Form->input('notify', array(
                    'label' => false,
                    'type' => 'select',
                    'options' => array(
                        0 => __('None'),
                        1 => __('Email on Run'),
                        2 => __('Email on Success'),
                        3 => __('Email on Fail')
                      )
                    )
                  ); ?>
               </div>
            </div>
  
            <div class="row">
              <div class="span2"><?php echo $this->Html->tag('b', __('Status')); ?></div>
              <div class="span10"><?php echo $this->Form->input('status', array(
                    'label' => false,
                    'type' => 'select',
                    'options' => array(
                        0 => __('Off'),
                        1 => __('Ready'),
                        2 => __('Running'),
                        3 => __('Failed')
                      )
                    )
                  ); ?>
               </div>
             </div>

            </div>

          </fieldset>
          
      <?php echo $this->Form->end(__('Add Job')); ?>
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
  
    </div>
  </div>
</div>