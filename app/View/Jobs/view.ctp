<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Jobs/view.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/jobs');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="jobs form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('Job'); ?>: <?php echo h($job['Job']['jobname']); ?></b></legend>
      </fieldset>

      <div class="container">
  
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Job Name')); ?></div>
          <div class="span10"><?php echo $job['Job']['jobname']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Time Pattern')); ?></div>
          <div class="span10"><?php echo $job['Job']['timepattern']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Command')); ?></div>
          <div class="span10"><?php echo $job['Job']['command']; ?></div>
        </div>
  
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Notify')); ?></div>
          <div class="span10"><?php echo $this->Job->jobNotifyLabel($job['Job']['notify']); ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Status')); ?></div>
          <div class="span10"><?php echo $this->Job->jobStatusLabel($job['Job']['status']); ?></div>
        </div>

        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>

      </div>
      
    </div>
  </div>
</div>