<!-- File: /app/View/Roles/index.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/roles');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
    
<div class="container-fluid center">
  <div class="roles index">
   
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Roles'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Role')); ?></p>
      </div>
     
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
          <th><?php echo $this->Paginator->sort('id');?></th>
          <th><?php echo $this->Paginator->sort('rolename', __('Role Name'));?></th>
          <th><?php echo $this->Paginator->sort('description', __('Description'));?></th>
          <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pread', __('Read'));?></th>
          <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pwrite', __('Write'));?></th>
          <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pexecute', __('Execute'));?></th>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($roles as $role): ?>
          <tr>
            <td><?php echo h($role['Role']['id']); ?></td>
            <td><?php echo $this->Html->link($role['Role']['rolename'], array('action' => 'view', $role['Role']['id'])); ?></td>
            <td><?php echo h($role['Role']['description']); ?></td>
            <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($role['Role']['pread']); ?></td>
            <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($role['Role']['pwrite']); ?></td>
            <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($role['Role']['pexecute']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($role['Role']['id']); ?>
              <?php echo $this->Button->deleteButton($role['Role']['id'], $role['Role']['rolename']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    
    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} roles out of {:count} total, starting on role {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
