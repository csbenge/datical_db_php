<!-- File: /app/View/Roles/add.ctp -->
  
<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/roles');
  $this->end();
?>

<br/>
  
<div class="container-fluid center">
  <div class="roles form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Role'); ?>
          <fieldset>
            <legend><b><?php echo __('Add Role'); ?></b></legend>
            
            <div class="container">   
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Role Name')); ?></div>
               <div class="span10"><?php echo $this->Form->input('rolename', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
               <div class="span10"><?php echo $this->Form->input('description', array('label' => false)); ?></div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Read')); ?></div>
               <div class="span10"><?php echo $this->Form->input('pread', array(
                    'label' => false,
                    'type' => 'select',
                    'options' => array(
                        0 => __('No'),
                        1 => __('Yes')
                      )
                    )
                  ); ?>
               </div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Write')); ?></div>
               <div class="span10"><?php echo $this->Form->input('pwrite', array(
                    'label' => false,
                    'type' => 'select',
                    'options' => array(
                        0 => __('No'),
                        1 => __('Yes')
                      )
                    )
                  ); ?>
               </div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Execute')); ?></div>
               <div class="span10"><?php echo $this->Form->input('pexecute', array(
                    'label' => false,
                    'type' => 'select',
                    'options' => array(
                        0 => __('No'),
                        1 => __('Yes')
                      )
                    )
                  ); ?>
               </div>
             </div>
             
            </div>
            
             
          </fieldset>
      <?php echo $this->Form->end(__('Add Role')); ?>
      
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>

