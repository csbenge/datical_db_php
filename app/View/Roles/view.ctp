<!-- File: /app/View/Roles/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/roles');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="roles form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('Role'); ?>: <?php echo h($role['Role']['rolename']); ?></b></legend>
      </fieldset>
      
      <div class="container">   
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Role')); ?></div>
          <div class="span10"><?php echo $role['Role']['rolename']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
          <div class="span10"><?php echo $role['Role']['description']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Read')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($role['Role']['pread']); ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Write')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($role['Role']['pwrite']); ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Execute')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($role['Role']['pexecute']); ?></div>
        </div>
      
        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>
      </div>
      
    </div>
  </div>
</div>