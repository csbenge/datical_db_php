<?php

/**
 * default layout
 *
 * This file is application-wide controller file.
 * 
 * @copyright     Copyright (c) 2013 Datical, Inc.
 * @license				All Rights Reserved
 * @link          hhttp://www.datical.com
 * @since         2013
 */

$appDescription = __d('cake_dev', 'Datical');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $appDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<link rel="icon" href="<?php echo $this->webroot . 'img/favicon.png'; ?>" type="image/png">
	<link rel="shortcut icon" href="<?php echo $this->webroot . 'img/favicon.png'; ?>" type="image/png">
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap-responsive');
		
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('jquery.popupWindow.js');
		echo $this->Html->script('cakebootstrap.js');
    echo $this->Js->link('prototype');  
    echo $this->Js->link('scriptaculous.js?load=effects');  
		echo $this->Html->css('custom');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

</head>
<body>

  <!-- HEADER -->
	
	<div id="container1" class="container-fluid">
		<div class="navbar navbar-fixed-top">  
		  <div class="navbar-inner">  
				<div class="container">  
			
				<!--TOP NAV-->
				
		    <ul class="nav">  
					<li class="active">  
						<a class="brand" href="/"><?php echo $this->Html->image('datical1.png', array('alt' => 'Datical', 'width' => '20'))?>  Datical</a>  
					</li>
					<?php if(isset($username)) :?>
						<li><a href="/databaseprojects"><?php echo __('Projects'); ?></a></li>
						<?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
							<li><a href="/databases"><?php echo __('Databases'); ?></a></li>
							<li><a href="#"><?php echo __('|'); ?></a></li>
							<li><a href="/jobs"><?php echo __('Jobs'); ?></a></li>
							<li><a href="/users"><?php echo __('Users'); ?></a></li>
						<?php endif; ?>
						<?php if ($userrole == "Admin") :?>
							<li><a href="/users"><?php echo __('Settings'); ?></a></li>
							<?php endif; ?>
						<li><a href="/users/logout"><?php echo __('Logout'); ?></a></li>
					<?php else :?>
	
					<?php endif; ?>
		    </ul>
	
				<div id="welcome">
					<?php if (isset($username)) :?>
						<p class="navbar-text pull-right">
						<?php echo __('User'); ?>:<b> <?php echo $username; ?> </b><?php echo __('Role'); ?>: <b><?php echo $userrole; ?></b>
						</p>
					<?php endif; ?>
				</div>
				</div>  
		  </div>  
		</div>
	</div>

  <!-- MAIN GRID -->

	<div id="container2" class="container-fluid" style="padding-top: 20px;">
		<div class="row-fluid">
	
			<!-- SIDE NAV -->
								 
			<div class="span2"><br/>
				<?php if (isset($username)) :?>
				<ul class="nav nav-list well">
					<?php
						$this->start('sidebar');
						$this->end();
						echo $this->fetch('sidebar');
					?>
				</ul>
	
				<?php endif; ?>
	
			<!-- COPYR -->
	
				<div id="container3" class="container-fluid">
				 <div id="footer">
					 <div id="container4" class="container-fluid center">
						 <br>
						 <table class="table table-condensed">
						 <tr>
							 <td>
							 <center>
							 <?php echo $this->Html->link(
									 $this->Html->image('datical_logo.png', array('alt' => $appDescription, 'border' => '0')),
									 'http://www.datical.com/',
									 array('target' => '_blank', 'escape' => false)
								 );
							 ?>
							 </center>
							 </td>
						 </tr>
						 </table>
					 </div>
				 </div>
			 </div>
	
			</div>
	
			<!-- CONTENT -->

			<div class="span10">
				<?php echo $this->Session->flash('auth'); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</div>
	
  <!-- FOOTER -->

</body>
</html>
