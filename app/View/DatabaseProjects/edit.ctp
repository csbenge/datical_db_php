<!-- File: /app/View/Projects/edit.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databaseprojects');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="users form">
    <div class="row well" style="vertical-align: middle;">    <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Project'); ?>
        <fieldset>
          <legend><b><?php echo __('Edit Project'); ?></b></legend>
          
        <div class="container">   
          <div class="row">
            <div class="span2"><?php echo $this->Html->tag('b', __('Project Name')); ?>&nbsp;&nbsp;</div>
            <div class="span10"><?php echo $this->Form->input('projectname', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
          </div>
          <div class="row">
            <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?>&nbsp;&nbsp;</div>
            <div class="span10"><?php echo $this->Form->input('description', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
          </div>

        </fieldset>
      
        <?php $options = array(
            'label' => __('Update Project'),
            'class' => array(
              'class' => 'btn btn-small btn-primary'
            )
          );
          echo $this->Form->end($options);
        ?>
        <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
      </div>
    </div>
  </div>
</div>


