<!-- File: /app/View/Projects/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databaseproject_details');
  $this->end();
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script>
  $(document).ready(function() {
     $('#Popup*').click(function() {
       var NWin = window.open($(this).prop('href'), '', 'height=600,width=800,scrollbars=1,resizable=1');
       if (window.focus)
       {
         NWin.focus();
       }
       return false;
      });
  });
  
  function toggle($div, $switchTextTag){
      var div1 = document.getElementById($div)
      var textEle = document.getElementById($switchTextTag);
      if (div1.style.display == 'none') {
          div1.style.display = 'block'
          textEle.innerHTML = 'Hide';
          
      } else {
          div1.style.display = 'none'
          textEle.innerHTML = 'Show';
      }
  }
</script>

<br/>
<div class="container-fluid center">
 
    <div class="row" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b>Project: <?php echo h($project['Project']['projectname']); ?></b></legend>
      </fieldset>
    </div>
    
    <div class="users form">

    <table class="table table-hover table-bordered table-condensed">
    <tr>
      <th>Description</th>
      <th>Owner</th>
      <th>Created</th>
      <th>Modified</th>
      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
      <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
      <?php endif; ?>
    </tr>
    
    <tr>
      <td><?php echo $project['Project']['description']; ?></td>
      <td><?php echo $project['User']['username']; ?></td>
      <td><?php echo $project['Project']['created']; ?></td>
      <td><?php echo $project['Project']['modified']; ?></td>
      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
      <td class="actions" style="text-align: center;">
          <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id']), array('class' => 'btn btn-mini')); ?>
          <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete "%s"?', $project['Project']['projectname'])); ?>
      </td>
      <?php endif; ?>
    </tr>
    </table>
    
    <!--------------------
          Deployment Plan
    --------------------->
    
    <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <h4>Deployment Plan</h4>
      </div>
    </div>

    <div id="changelog" style="text-align: center;">
      <table style="text-align: center;">
      <tr style="text-align: center;">
        
    <?php
      foreach ($dp_parsed_xml["Project"]["dbDefs"] as $dbDef):
        $name     = $dbDef["@name"];
        $driver   = $dbDef["@driver"];
        $hostname = $dbDef["@hostname"];
        $port     = $dbDef["@port"];
        $username = $dbDef["@username"];
        $database = $dbDef["@database"];
    ?>

        <td width="25%">
            <table class="table table-hover table-bordered table-condensed">
             <tr> 
               <td colspan=2 style="text-align: center;">
                <b><?php echo $name; ?></b><br/>
                <b><?php echo $this->Datical->daticalDBStatus($database); ?></b><br/>
                <?php echo $this->Html->link('Connect', array('controller' => 'Projects', 'action' => 'connect'), array('class' => 'btn btn-mini')); ?>
                <?php echo $this->Html->link('Snapshot', array('controller' => 'Projects', 'action' => 'snapshot'), array('class' => 'btn btn-mini')); ?>
                <?php echo $this->Html->link('Forecast', array('controller' => 'Projects', 'action' => 'forecast') , array('class' => 'btn btn-mini')); ?>
                <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                  <?php echo $this->Html->link('Deploy', array('controller' => 'Projects', 'action' => 'deploy'), array('class' => 'btn btn-mini')); ?>
                <?php endif; ?>
               </td>
             </tr>
             <tr>
               <td style="width:50%">Host</td><td style="width:50%"><?php echo $hostname; ?></td>
             </tr>
             <tr>
               <td>Port</td><td><?php echo $port; ?></td>
             </tr>
             <tr>
               <td>User</td><td><?php echo $username; ?></td>
             </tr>
             <tr>
               <td>Database</td><td><?php echo $database; ?></td>
             </tr>
             <tr>
               <td>Driver</td><td><?php echo $driver; ?></td>
             </tr>
            </table>
        </td>
  <?php endforeach; ?>
  
      </tr>
      </table>
    </div>
  
    <!--------------------
          Changelog
    --------------------->
    
    <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <h4>Changelog</h4>
      </div>
    </div>

    <div id="changelog" style="display:block;">
      
    <table class="table table-hover table-bordered table-condensed">
      <tr>
        <th>Name</th>
        <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        <?php endif; ?>
       </tr>
        <tr>
          <td><?php echo $this->Html->link($cl_filename, "$cl_filedir/$cl_filename", array('id' => "Popup")); ?></td>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
            <td class="actions" style="text-align: center;">
             <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id']), array('class' => 'btn btn-mini')); ?>
             <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('class' => 'btn btn-mini'), null, __('Are you sure you want to delete project: "%s"?', $project['Project']['projectname'])); ?>
            </td>
          <?php endif; ?>
        </tr>
       </table>
     
    </div>
  
    <!--------------------
          Reports
    --------------------->
  
    <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <h4>Reports - <small><a id="reportsLink" href="#" type=link onClick="javascript:toggle('reports', 'reportsLink')">Show</a></small></h4>
      </div>
    </div>
    
    <div id="reports" style="display:none;">
  
     <table class="table table-hover table-bordered table-condensed">
       <tr>
         <th>Deploys</th>
         <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
         <?php endif; ?>
       </tr>
       <?php
         foreach ($rp_filelist[0] as $file):
          if (preg_match("/^forecast/", $file)) {
            continue;
          }
           $file_abs = $rp_filedir . $file;
        ?>
        <tr>
          <td><?php echo $this->Html->link($file, "$rp_filedir/$file/DeployReport.html", array('id' => "Popup")); ?></td>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
          <td class="actions" style="text-align: center;">
           <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id']), array('class' => 'btn btn-mini')); ?>
           <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('class' => 'btn btn-mini'), null, __('Are you sure you want to delete project: "%s"?', $project['Project']['projectname'])); ?>
          </td>
          <?php endif; ?>
        <?php endforeach; ?> 
        </tr>
       </table>

      <table class="table table-hover table-bordered table-condensed">
       <tr>
         <th>Forecasts</th>
         <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        <?php endif; ?>
       </tr>
       <?php
         foreach ($rp_filelist[0] as $file):
          if (preg_match("/^deploy/", $file)) {
            continue;
          }
           $file_abs = $rp_filedir . $file;
        ?>
        <tr>
          <td><?php echo $this->Html->link($file, "$rp_filedir/$file/forecastReportTemplate.html", array('id' => "Popup")); ?></td>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
            <td class="actions" style="text-align: center;">
             <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id']), array('class' => 'btn btn-mini')); ?>
             <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete "%s"?', $project['Project']['projectname'])); ?>
            </td>
          <?php endif; ?>
        <?php endforeach; ?> 
        </tr>
       </table>
  
    </div>
  
    <!--------------------
          Snapshots
    --------------------->

    <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <h4>Snapshots - <small><a id="snapshotsLink" href="#" type=link onClick="javascript:toggle('snapshots', 'snapshotsLink')">Show</a></small></h4>
      </div>
    </div>

    <div id="snapshots" style="display:none;">

     <table class="table table-hover table-bordered table-condensed">
       <tr>
         <th>Name</th>
         <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        <?php endif; ?>
       </tr>
       <?php
         foreach ($sn_filelist as $file):
           $file_abs = WWW_ROOT . $sn_filedir . $file;
           $parsed_xml = Xml::build($file_abs); 
           $parsed_xml = Set::reverse($parsed_xml);
        ?>
        <tr>
          <td><?php echo $this->Html->link($file, "$sn_filedir/$file", array('id' => "Popup")); ?></td>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
            <td class="actions" style="text-align: center;">
             <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id']), array('class' => 'btn btn-mini')); ?>
             <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('class' => 'btn btn-mini'), null, __('Are you sure you want to delete project: "%s"?', $project['Project']['projectname'])); ?>
            </td>
          <?php endif; ?>
        <?php endforeach; ?> 
        </tr>
       </table>

    </div>

   </div>
  
</div>