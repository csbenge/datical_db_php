<!-- File: /app/View/DatabaseProjects/index.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databaseprojects');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>

<div class="container-fluid center">
  <div class="projects index">
   
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
          <legend><b><?php echo __('Projects'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
           <?php if (($userrole == "Manager") || $userrole == "Admin") :?>
              <p><?php echo $this->Button->addButton(__('Add Project')); ?></p>
          <?php endif; ?>
      </div>
   
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
          <th><?php echo $this->Paginator->sort('id');?></th>
          <th><?php echo $this->Paginator->sort('projectname', __('Project Name'));?></th>
          <th><?php echo $this->Paginator->sort('description');?></th>
          <th><?php echo $this->Paginator->sort('created');?></th>
          <th><?php echo $this->Paginator->sort('modified');?></th>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
            <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
          <?php endif; ?>
        </tr>

        <?php
        $i = 0;
        foreach ($projects as $project): ?>
        <tr>
          <td><?php echo $project['DatabaseProject']['id']; ?></td>
          <td><?php echo $this->Html->link($project['DatabaseProject']['projectname'], array('action' => 'view', $project['DatabaseProject']['id'])); ?></td>
          <td><?php echo $project['DatabaseProject']['description']; ?></td>
          <td><?php echo $project['DatabaseProject']['created']; ?></td>
          <td><?php echo $project['DatabaseProject']['modified']; ?></td>
          <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($project['DatabaseProject']['id']); ?>
              <?php echo $this->Button->deleteButton($project['DatabaseProject']['id'], $project['DatabaseProject']['projectname']); ?>
            </td>
          <?php endif; ?>
        </tr>
        <?php endforeach; ?>
      </table>
  
    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} projects out of {:count} total, starting on project {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>
  </div>
</div>
