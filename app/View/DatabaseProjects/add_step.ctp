<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Projects/add_step.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databaseprojects');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
  
<div class="container-fluid center">
  <div class="databases form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('DatabaseRoute'); ?>
        <fieldset>
          <legend><b><?php echo __('Add Step'); ?></b></legend>

          <div class="container">

            <div class="row">
              <div class="span2"><?php echo $this->Html->tag('b', __('Project Name')); ?>&nbsp;&nbsp;</div>
              <div class="span10"><?php echo h($project['DatabaseProject']['projectname']); ?></div>
            </div>
            
            <div class="row">
              <div class="span2"><?php echo $this->Html->tag('b', __('Database')); ?></div>
              <div class="span10"><?php echo $this->Form->input('db_id', array(
                   'label' => false,  
                   'type' => 'select',
                   'options' => $database_list
                 )
               ); ?>
              </div>
           </div>
            
            <div class="row">
              <div class="span2"><?php echo $this->Html->tag('b', __('Order')); ?></div>
              <div class="span10"><?php echo $this->Form->input('dbroute_step', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
            </div>

          </div>

        </fieldset>
          
      <?php echo $this->Form->end(__('Add Step')); ?>
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
  
    </div>
  </div>
</div>