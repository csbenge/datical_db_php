<!-- File: /app/View/DatabaseProjects/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databaseproject_view');
  $this->end();
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script>
  $(document).ready(function() {
     $('#Popup*').click(function() {
       var NWin = window.open($(this).prop('href'), '', 'height=600,width=800,scrollbars=1,resizable=1');
       if (window.focus)
       {
         NWin.focus();
       }
       return false;
      });
  });
  
  function toggleReports($div, $switchTextTag){
      var div1 = document.getElementById($div)
      var textEle = document.getElementById($switchTextTag);
      if (div1.style.display == 'none') {
          div1.style.display = 'block'
          textEle.innerHTML = '- Reports';
          
      } else {
          div1.style.display = 'none'
          textEle.innerHTML = '+ Reports';
      }
  }
  
  function toggleSnapshots($div, $switchTextTag){
      var div1 = document.getElementById($div)
      var textEle = document.getElementById($switchTextTag);
      if (div1.style.display == 'none') {
          div1.style.display = 'block'
          textEle.innerHTML = '- Snapshots';
          
      } else {
          div1.style.display = 'none'
          textEle.innerHTML = '+ Snapshots';
      }
  }
</script>

<br/>
<div class="container-fluid center">
 
    <div class="row" style="vertical-align: middle;">
      <div class="span9">
           <fieldset>
        <legend><b><?php echo __('Project') ?>: <?php echo h($project['DatabaseProject']['projectname']); ?></b></legend>
      </fieldset>
      </div>
    </div>
    
    <div class="users form">

    <table class="table table-hover table-bordered table-condensed table-striped">
    <tr>
      <th style="background-color: #e5e5e5;width: 200px"><?php echo __('Description') ?></th>
      <th style="background-color: #e5e5e5;width: 200px"><?php echo __('Owner') ?></th>
      <th style="background-color: #e5e5e5;width: 200px"><?php echo __('Created') ?></th>
      <th style="background-color: #e5e5e5;width: 200px"><?php echo __('Modified') ?></th>
      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
      <th class="actions" style="text-align: center;background-color: #e5e5e5;width: 200px"><?php echo __('Actions');?></th>
      <?php endif; ?>
    </tr>
  
    <tr>
      <td><?php echo $project['DatabaseProject']['description']; ?></td>
      <td><?php echo $project['User']['username']; ?></td>
      <td><?php echo $project['DatabaseProject']['created']; ?></td>
      <td><?php echo $project['DatabaseProject']['modified']; ?></td>
      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
      <td class="actions" style="text-align: center;">
          <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['DatabaseProject']['id']), array('class' => 'btn btn-mini')); ?>
          <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['DatabaseProject']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete "%s"?', $project['DatabaseProject']['projectname'])); ?>
      </td>
      <?php endif; ?>
    </tr>
    </table>
  
    <!--------------------
          Deployment Plan
    --------------------->
  
    <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <h4><?php echo __('Deployment Plan') ?></h4>
      </div>
       <div class="span3" style="text-align: right;">
        <br/>
           <?php if (($userrole == "Manager") || $userrole == "Admin") :?>
           <?php $path = '/databaseprojects/add_step/' . $project['DatabaseProject']['id'] ?>
              <p><?php echo $this->Button->addButtonPath(__('Add Step'), $path); ?></p>
          <?php endif; ?>
      </div>
    </div>

    <div id="changelog" style="text-align: center;">
      <table class="table table-hover table-bordered table-condensed table-striped">
      <tr>
        <th style="background-color: #e5e5e5;width: 400px"><?php echo __('Step') ?></th>
        <th style="background-color: #e5e5e5;width: 200px"><?php echo __('Current ChangeSet') ?></th>
        <th style="background-color: #e5e5e5;width: 150px"><?php echo __('Created') ?></th>
        <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
        <th class="actions" style="text-align: center;background-color: #e5e5e5;width: 350px"><?php echo __('Actions');?></th>
        <?php endif; ?>
      </tr>

      <?php $i=0; ?>
      <?php foreach ($db_routes as $db_route): ?>
  
          <tr>
  
            <td>
  
              <b><?php echo $this->Datical->daticalDBStatusLabel($db_route['DatabaseRoute']['Database']['db_name'],
                                                                 $db_route['DatabaseRoute']['Database']['db_status']); ?></b>
  
              <!-- Reports -->
  
              <div>
                <a id="<?php echo 'reportsLink' . $i; ?>" href="#" type=link onClick="javascript:toggleReports(<?php echo '\'' . 'reports' . $i . '\''; ?>, <?php echo '\'' . 'reportsLink' . $i . '\''; ?>)">+ <?php echo __('Reports') ?></a></small></h4>
              </div>

              <div id="<?php echo 'reports' . $i; ?>" style="display:none;">
               <table class="table table-hover table-bordered table-condensed">
                <tr>
                  <th><?php echo __('Name') ?></th>
                  <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                    <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
                  <?php endif; ?>
                </tr>
                <?php foreach ($db_route['DatabaseRoute']['Database']['DatabaseReport'] as $db_report): ?>
                  <tr>
                    <td><?php echo $this->Html->link($db_report['dbrep_name'], $db_report['dbrep_file'], array('id' => "Popup")); ?></td>
                      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                        <td class="actions" style="text-align: center;">
                          <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete_report', $db_report['id']), array('class' => 'btn btn-mini'), null, __('Are you sure you want to delete project: "%s"?', $db_report['dbrep_name'])); ?>
                        </td>
                      <?php endif; ?>
                    </tr>
                <?php endforeach; ?> 
                </table>
              </div>
  
              <!-- Snapshots -->
  
              <div>
                <a id="<?php echo 'snapshotsLink' . $i; ?>" href="#" type=link onClick="javascript:toggleSnapshots(<?php echo '\'' . 'snapshots' . $i . '\''; ?>, <?php echo '\'' . 'snapshotsLink' . $i . '\''; ?>)">+ <?php echo __('Snapshots') ?></a></small></h4>
              </div>
            
              <div id="<?php echo 'snapshots' . $i; ?>" style="display:none;">
               <table class="table table-hover table-bordered table-condensed">
                <tr>
                  <th><?php echo __('Name') ?></th>
                  <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                    <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
                  <?php endif; ?>
                </tr>
                <?php foreach ($db_route['DatabaseRoute']['Database']['DatabaseSnapshot'] as $db_snapshot): ?>
                  <tr>
                    <td><?php echo $this->Html->link($db_snapshot['dbsnap_name'], $db_snapshot['dbsnap_file'], array('id' => "Popup")); ?></td>
                      <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                        <td class="actions" style="text-align: center;">
                          <?php echo $this->Form->postLink(__('Delete2'), array('action' => 'delete_snapshot', $db_snapshot['id']), array('class' => 'btn btn-mini'), null, __('Are you sure you want to delete snapshot: "%s"?', $db_snapshot['dbsnap_name'])); ?>
                        </td>
                      <?php endif; ?>
                    </tr>
                <?php endforeach; ?> 
                </table>
              </div>
                
            </td>
  
            <td>DATICALDB_API</td>
            <td>2013-03-08 14:21:52</td>
            
            <!-- Step Actions -->

            <td  class="actions" style="text-align: center;">
             <?php echo $this->Html->link(__('Connect'), array('controller' => 'DatabaseProjects', 'action' => 'connect'), array('class' => 'btn btn-mini')); ?>
             <?php echo $this->Html->link(__('Snapshot'), array('controller' => 'DatabaseProjects', 'action' => 'snapshot'), array('class' => 'btn btn-mini')); ?>
             <?php echo $this->Html->link(__('Forecast'), array('controller' => 'DatabaseProjects', 'action' => 'forecast') , array('class' => 'btn btn-mini')); ?>
             <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
               <?php echo $this->Html->link(__('Deploy'), array('controller' => 'DatabaseProjects', 'action' => 'deploy'), array('class' => 'btn btn-mini')); ?>
               <?php echo $this->Html->link(__('Rollback'), array('controller' => 'DatabaseProjects', 'action' => 'deploy'), array('class' => 'btn btn-mini')); ?>
             <?php endif; ?>
             <?php echo $this->Html->link(__('History'), array('controller' => 'DatabaseProjects', 'action' => 'deploy'), array('class' => 'btn btn-mini')); ?>
              <?php if (($userrole == "Manager") || ($userrole == "Admin")) :?>
                  <?php $path_edit = '/databaseprojects/edit_step/' . $db_route['DatabaseRoute']['Database']['db_name'] ?>
                  <?php echo $this->Button->deleteButtonAction('delete_step', $db_route['DatabaseRoute']['id'], $db_route['DatabaseRoute']['Database']['db_name']); ?>
                </td>
              <?php endif; ?>
            </td>
          </tr>

        <?php $i++; ?>
      <?php endforeach; ?>

      </table>
    </div>

  </div>
</div>
