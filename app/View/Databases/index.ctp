<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Databases/index.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>

<div class="container-fluid center">
  <div class="databases index">
  
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Databases'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Database')); ?></p>
      </div>
  
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
       <!--   <th style="background-color: #e5e5e5"><?php echo $this->Paginator->sort('id');?></th> -->
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('db_name', __('Name'));?></th>
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('db_name', __('Server'));?></th>
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('db_name', __('Environment'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('db_desc', __('Description'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('db_desc', __('User Name'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('db_desc', __('Password'));?></th>
          <th class="actions" style="text-align: center;background-color: #e5e5e5;width: 100px"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($databases as $database): ?>
          <tr>
         <!--   <td><?php echo h($database['Database']['id']); ?></td> -->
            <td><?php echo $this->Html->link($database['Database']['db_name'], array('action' => 'view', $database['Database']['id'])); ?></td>
            <td><?php echo h($database['DatabaseServer']['dbsvr_name']); ?></td>
            <td><?php echo $this->Database->databaseServerEnvLabel($database['Database']['db_env']); ?></td>
            <td><?php echo h($database['Database']['db_desc']); ?></td>
            <td><?php echo h($database['Database']['db_username']); ?></td>
            <td><?php echo h($database['Database']['db_password']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($database['Database']['id']); ?>
              <?php echo $this->Button->deleteButton($database['Database']['id'], $database['Database']['db_name']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>

    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} databases out of {:count} total, starting on database {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
