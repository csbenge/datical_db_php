<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Databases/view.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="databases form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('Database'); ?>: <?php echo h($database['Database']['db_name']); ?></b></legend>
      </fieldset>

      <div class="container">
  
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Name')); ?></div>
          <div class="span10"><?php echo $database['Database']['db_name']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Server')); ?></div>
          <div class="span10"><?php echo $database['DatabaseServer']['dbsvr_name']; ?></div>
        </div>
        
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Environment')); ?></div>
          <div class="span10"><?php echo $this->Database->databaseServerEnvLabel($database['Database']['db_env']); ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
          <div class="span10"><?php echo $database['Database']['db_desc']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('User Name')); ?></div>
          <div class="span10"><?php echo $database['Database']['db_username']; ?></div>
        </div>
         
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Password')); ?></div>
          <div class="span10"><?php echo $database['Database']['db_password']; ?></div>
        </div>

        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>

      </div>
      
    </div>
  </div>
</div>