<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Databases/edit.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="databases form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Database'); ?>
          <fieldset>
            <legend><b><?php echo __('Edit Database'); ?></b></legend>

             <div class="container">
              
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Name')); ?></div>
               <div class="span10"><?php echo $this->Form->input('db_name', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
             </div>
             
              <?php $dbsvr = $this->request->data['Database']['dbsvr_id']; ?>
              <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Server')); ?></div>
               <div class="span10"><?php echo $this->Form->input('dbsvr_id', array(
                    'label' => false,  
                    'type' => 'select',
                    'selected' => $dbsvr,
                    'options' => $server_list
                  )
                ); ?>
                </div>
             </div>
              
              <?php $dbenv = $this->request->data['Database']['db_env']; ?>
              <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Environment')); ?></div>
               <div class="span10"><?php echo $this->Form->input('db_env', array(
                    'label' => false,
                    'type' => 'select',
                    'selected' => $dbenv,
                    'style'=>'width:200px;',
                    'options' => array(
                        '1' => _('Development'),
                        '2' => __('QA'),
                        '3' => __('Stage'),
                        '4' => __('Production'),
                        '5' => __('Disaster Recovery')
                      )
                    )
                  ); ?>
               </div>
             </div>
              
            <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
               <div class="span10"><?php echo $this->Form->input('db_desc', array('label' => false)); ?></div>
             </div>
            
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('User Name')); ?></div>
               <div class="span10"><?php echo $this->Form->input('db_username', array('label' => false)); ?></div>
             </div>
             
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Password')); ?></div>
               <div class="span10"><?php echo $this->Form->input('db_password', array('label' => false)); ?></div>
             </div>
             
            </div>

          </fieldset>
      <?php echo $this->Form->end(__('Update Database')); ?>
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>