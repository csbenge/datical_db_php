<!-- File: /app/View/Groups/index.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/groups');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
    
<div class="container-fluid center">
  <div class="groups index">
   
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Groups'); ?></b></legend>
        </fieldset>
      </div>
      
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Group')); ?></p>
      </div>
     
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
          <th><?php echo $this->Paginator->sort('id');?></th>
          <th><?php echo $this->Paginator->sort('groupname', __('Group Name'));?></th>
          <th><?php echo $this->Paginator->sort('description', __('Description'));?></th>
          <th><?php echo $this->Paginator->sort('role', __('Role'));?></th>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($groups as $group): ?>
          <tr>
            <td><?php echo h($group['Group']['id']); ?></td>
            <td><?php echo $this->Html->link($group['Group']['groupname'], array('action' => 'view', $group['Group']['id'])); ?></td>
            <td><?php echo h($group['Group']['description']); ?></td>
            <td><?php echo h($group['Role']['rolename']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($group['Group']['id']); ?>
              <?php echo $this->Button->deleteButton($group['Group']['id'], $group['Group']['groupname']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    
    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} groups out of {:count} total, starting on group {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
