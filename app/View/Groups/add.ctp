<!-- File: /app/View/Groups/add.ctp -->
  
<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/groups');
  $this->end();
?>

<br/>
  
<div class="container-fluid center">
  <div class="groups form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Group'); ?>
          <fieldset>
            <legend><b><?php echo __('Add Group'); ?></b></legend>
            
            <div class="container">   
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Group Name')); ?></div>
               <div class="span10"><?php echo $this->Form->input('groupname', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
               <div class="span10"><?php echo $this->Form->input('description', array('label' => false)); ?></div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Role')); ?></div>
               <div class="span10"><?php echo $this->Form->input('role_id', array('type' => 'select', 'options' => $role_list, 'label' => false)); ?></div>
             </div>

            </div>
            
             
          </fieldset>
      <?php echo $this->Form->end(__('Add Group')); ?>
      
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>

