<!-- File: /app/View/Groups/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/groups');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>

<div class="container-fluid center">
  <div class="groups form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('Group'); ?>: <?php echo h($group['Group']['groupname']); ?></b></legend>
      </fieldset>
      
      <div class="container">   
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Group')); ?></div>
          <div class="span10"><?php echo $group['Group']['groupname']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
          <div class="span10"><?php echo $group['Group']['description']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Role')); ?></div>
          <div class="span10"><?php echo $group['Role']['rolename']; ?></div>
        </div>
         <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Members')); ?></div>
          <div class="span10"><?php echo $totalmembers; ?></div>
        </div>
      
        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>
      </div>
      
    </div>
  </div>
</div>

<!-- Show members of this group -->

<div class="container-fluid center">
  <div class="groupmembers form">
    <div class="row well" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Members'); ?></b></legend>
        </fieldset>
      </div>
      
      <div class="container">   
        <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButtonPath(__('Add Member'), '/groupmembers/add'); ?></p>
      </div>
     
      <table class="table table-hover table-bordered table-condensed table-striped">
      <tr>
        <th><?php echo __('id');?></th>
        <th><?php echo __('Group Name');?></th>
        <th><?php echo __('User Name');?></th>
        <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
      </tr>
      <?php
        foreach ($groupmembers as $member): ?>
        <tr>
          <td><?php echo h($member['GroupMember']['id']); ?></td>
          <td><?php echo h($member['Group']['groupname']); ?></td>
          <td><?php echo h($member['User']['username']); ?></td>
          <td class="actions" style="text-align: center;">
            <?php echo $this->Button->removeButtonGroupMember($member['GroupMember']['id'], $member['Group']['id'], $member['User']['username']); ?>
          </td>
        </tr>
      <?php endforeach; ?>
      </table>
      
        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>
      </div>
      
    </div>
  </div>
</div>