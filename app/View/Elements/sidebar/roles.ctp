<!-- File: /app/View/Elements/sidebar/roles.ctp -->

<li class="nav-header"><?php echo __('Users'); ?></li>
<li><?php echo $this->Html->link(__('Roles'), '/roles'); ?></li>
<li><?php echo $this->Html->link(__('Groups'), '/groups'); ?></li>
<li><?php echo $this->Html->link(__('Credentials'), '/credentials'); ?></li>
<br/>
