<!-- File: /app/View/Users/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/users');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="users form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('User'); ?>: <?php echo h($user['User']['username']); ?></b></legend>
      </fieldset>
      
        <div class="container">   
          <div class="row">
            <div class="span2"><?php echo $this->Html->tag('b', __('User Name')); ?></div>
            <div class="span10"><?php echo $user['User']['username']; ?></div>
          </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Password')); ?></div>
          <div class="span10"><?php echo $user['User']['password']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Base Role')); ?></div>
          <div class="span10"><?php echo $user['Role']['rolename']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Groups')); ?></div>
          <div class="span10"><?php echo $groups; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Created')); ?></div>
          <div class="span10"><?php echo $user['User']['created']; ?></div>
        </div>
        
        <div class="row">
          <?php echo $this->Html->link(__('Back',true),"javascript:history.back()"); ?>
        </div>
      </div>
  
    </div>
  </div>
</div>