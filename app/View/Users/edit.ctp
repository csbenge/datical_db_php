<!-- File: /app/View/Users/edit.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/users');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="users form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('User'); ?>
          <fieldset>
            <legend><b><?php echo __('Edit User'); ?></b></legend>
            
            <div class="container">   
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('User Name')); ?></div>
                <div class="span10"><?php echo $this->Form->input('username', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
              </div>
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Password')); ?></div>
                <div class="span10"><?php echo $this->Form->input('password', array('label' => false)); ?></div>
              </div>
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Password Confirm')); ?>&nbsp;&nbsp;</div>
                <div class="span10"><?php echo $this->Form->input('password_confirm', array('label' => false, 'type' => 'password')); ?></div>
              </div>
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Base Role')); ?></div>
                <div class="span10"><?php echo $this->Form->input('role_id', array('type' => 'select', 'options' => $role_list, 'label' => false)); ?></div>
              </div>
            </div>
            
          </fieldset>
          <?php $options = array(
              'label' => __('Update User'),
              'class' => array(
                'class' => 'btn btn-small btn-primary'
              )
            );
            echo $this->Form->end($options);
          ?>
          <?php echo $this->Html->link(__('Cancel',true),"javascript:history.back()"); ?>
    </div>
  </div>
</div>