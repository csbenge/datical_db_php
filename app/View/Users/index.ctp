<!-- File: /app/View/Users/index.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/users');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
    
<div class="container-fluid center">
  <div class="users index">
   
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Users'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
        <p><?php echo $this->Button->addButton(__('Add User')); ?></p>
      </div>
     
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr class="success">
          <th><?php echo $this->Paginator->sort('id');?></th>
          <th><?php echo $this->Paginator->sort('username', __('User Name'));?></th>
          <th><?php echo $this->Paginator->sort('password');?></th>
          <th><?php echo $this->Paginator->sort('role', __('Base Role'));?></th>
          <th><?php echo $this->Paginator->sort('created');?></th>
          <th><?php echo $this->Paginator->sort('modified');?></th>
          <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($users as $user): ?>
          <tr>
            <td><?php echo h($user['User']['id']); ?></td>
            <td><?php echo $this->Html->link($user['User']['username'], array('action' => 'view', $user['User']['id'])); ?></td>
            <td><?php echo h($user['User']['password']); ?></td>
            <td><?php echo h($user['Role']['rolename']); ?></td>
            <td><?php echo h($user['User']['created']); ?></td>
            <td><?php echo h($user['User']['modified']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($user['User']['id']); ?>
              <?php echo $this->Button->deleteButton($user['User']['id'], $user['User']['username']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    
    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} users out of {:count} total, starting on user {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
