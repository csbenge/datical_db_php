<!-- File: /app/View/Users/login.ctp -->

<div class="container center" style="padding-left:250px;">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
   <br/>
    <div class="users form center well" style="width:500px">
    <div style="text-align:center;"><h3>Datical DB<br/><?php echo __('Login'); ?></h3></div>
    <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
    
    <div class="control-group">
      <label class="control-label"><?php echo __('User Name'); ?></label>
      <div class="controls">
        <?php echo $this->Form->input('username', array('label' => false, 'placeholder' => __('User Name'), 'autofocus'=>'autofocus')); ?>
      </div>
    </div>
    
    <div class="control-group">
      <label class="control-label"><?php echo __('Password'); ?></label>
      <div class="controls">
        <?php echo $this->Form->input('password', array('label' => false, 'placeholder' => __('Password'))); ?>
      </div>
    </div>
    
    <div class="control-group" style="padding-left:170px;">
      <div class="controls">
        <?php $options = array(
          'label' => __('Login'),
          'class' => array(
            'class' => 'btn btn-small btn-primary'
          )
        );
        echo $this->Form->end($options);
        ?>
      </div>
    </div>
  
    </div>
</div>
