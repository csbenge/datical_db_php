<!-- File: /app/View/Credentials/add.ctp -->
  
<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/credentials');
  $this->end();
?>

<br/>
  
<div class="container-fluid center">
  <div class="credentials form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Credential'); ?>
          <fieldset>
            <legend><b><?php echo __('Add Credential'); ?></b></legend>
            
            <div class="container">   
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Project')); ?></div>
               <div class="span10"><?php echo $this->Form->input('project_id', array(
                    'label' => false,  
                    'type' => 'select',
                    'options' => $project_list
                  )
                ); ?>
                </div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('User')); ?></div>
               <div class="span10"><?php echo $this->Form->input('user_id', array(
                    'label' => false,  
                    'type' => 'select',
                    'options' => $user_list
                  )
                ); ?>
                </div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Group')); ?></div>
               <div class="span10"><?php echo $this->Form->input('group_id', array(
                    'label' => false,  
                    'type' => 'select',
                    'options' => $group_list
                  )
                ); ?>
                </div>
             </div>
             <div class="row">
               <div class="span2"><?php echo $this->Html->tag('b', __('Role')); ?></div>
               <div class="span10"><?php echo $this->Form->input('role_id', array(
                    'label' => false,  
                    'type' => 'select',
                    'options' => $role_list
                  )
                ); ?>
                </div>
             </div>
             
            </div>
            
          </fieldset>
          <?php echo $this->Form->end(__('Add Credential')); ?>
          
          <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>

