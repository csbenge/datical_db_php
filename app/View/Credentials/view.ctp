<!-- File: /app/View/Credentials/view.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/credentials');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="credentials form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('Credential'); ?>
      <fieldset>
        <legend><b><?php echo __('Credential'); ?></legend>
      </fieldset>

      <div class="container">   
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Project')); ?></div>
          <div class="span10"><?php echo $credential['DatabaseProject']['projectname']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('User')); ?></div>
          <div class="span10"><?php echo $credential['User']['username']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Group')); ?></div>
          <div class="span10"><?php echo $credential['Group']['groupname']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Role')); ?></div>
          <div class="span10"><?php echo $credential['Role']['rolename']; ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Read')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($credential['Role']['pread']); ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Write')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($credential['Role']['pwrite']); ?></div>
        </div>
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Execute')); ?></div>
          <div class="span10"><?php echo $this->Role->getPermText($credential['Role']['pexecute']); ?></div>
        </div>

        <div class="row">
          <?php echo $this->Html->link(__('Back',true),"javascript:history.back()"); ?>
        </div>
      </div>
        
    </div>
  </div>
</div>