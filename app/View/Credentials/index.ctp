<!-- File: /app/View/Credentials/index.ctp -->

<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/credentials');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>
    
<div class="container-fluid center">
  <div class="credentials index">
   
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Credentials'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Credential')); ?></p>
      </div>
     
    <table class="table table-hover table-bordered table-condensed table-striped">
      <tr>
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('project_id', __('Project'));?></th>
        <th><?php echo $this->Paginator->sort('user_id', __('User'));?></th>
        <th><?php echo $this->Paginator->sort('group_id', __('Group'));?></th>
        <th><?php echo $this->Paginator->sort('role_id', __('Role'));?></th>
        <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pread', __('Read'));?></th>
        <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pwrite', __('Write'));?></th>
        <th class="actions" style="text-align: center;"><?php echo $this->Paginator->sort('pexecute', __('Execute'));?></th>
        <th class="actions" style="text-align: center;"><?php echo __('Actions');?></th>
      </tr>
      <?php
        $i = 0;
        foreach ($credentials as $credential): ?>
        <tr>
          <td><?php echo h($credential['Credential']['id']); ?></td>
          <td><?php echo $this->Html->link($credential['DatabaseProject']['projectname'], array('action' => 'view', $credential['Credential']['id'])); ?></td>
          <td><?php echo $credential['User']['username'] ?></td>
          <td><?php echo $credential['Group']['groupname'] ?></td>
          <td><?php echo $credential['Role']['rolename'] ?></td>
          <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($credential['Role']['pread']); ?></td>
          <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($credential['Role']['pwrite']); ?></td>
          <td class="actions" style="text-align: center;"><?php echo $this->Role->getPermText($credential['Role']['pexecute']); ?></td>
          <td class="actions" style="text-align: center;">
            <?php echo $this->Button->editButton($credential['Credential']['id']); ?>
            <?php echo $this->Button->deleteButton($credential['Credential']['id'], $credential['Credential']['id']); ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </table>
    
    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} credentials out of {:count} total, starting on credential {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
