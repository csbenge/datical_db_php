<!-- File: /app/View/GroupMembers/add.ctp -->
  
<?php
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/groups');
  $this->end();
?>

<br/>
  
<div class="container-fluid center">
  <div class="groupmemberss form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('GroupMember'); ?>
      <fieldset>
        <legend><b><?php echo __('Add Member'); ?></b></legend>
        
        <div class="container">   
         <div class="row">
           <div class="span2"><?php echo $this->Html->tag('b', __('User Name')); ?></div>
           <div class="span10"><?php echo $this->Form->input('user_id', array('type' => 'select', 'options' => $user_list, 'label' => false)); ?></div>
         </div>
          <div class="row">
           <div class="span2"><?php echo $this->Html->tag('b', __('Group Name')); ?></div>
           <div class="span10"><?php echo $this->Form->input('group_id', array('type' => 'select', 'options' => $group_list, 'label' => false)); ?></div>
         </div>
        </div>
      </fieldset>
      <?php echo $this->Form->end(__('Add Member')); ?>
      
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>

