<?php

/* /app/View/Helper/DaticalHelper.php (using other helpers) */

App::uses('AppHelper', 'View/Helper');

require_once(APP . 'Console/Command' . DS . 'tdcron.php');

class DaticalHelper extends AppHelper {
  public $helpers = array('Html', 'Form');

  public function daticalDBStatus($database) {

  // Call CLI to get status
    
    $dbstatus = 1;
    switch ($dbstatus) {
      case 0:
        $html = $this->Html->image('db_status_green.png');
        break;
      case 1:
        $html = $this->Html->image('db_status_yellow.png');
        break;
      case 2:
        $html = $this->Html->image('db_status_red.png');
        break;
    }
      return $html;
  }
  
  public function daticalDBStatusLabel($dbname, $dbstatus) {
    
    switch ($dbstatus) {
      case 0:
        $html = "<span class='label label'>$dbname</span>";
        break;
      case 1:
        $html = "<span class='label label-info'>$dbname</span>";
        break;
      case 2:
        $html = "<span class='label label-success'>$dbname</span>";
        break;
      case 3:
        $html = "<span class='label label-important'>$dbname</span>";
        break;
    }
    return $html;
  }
  
}