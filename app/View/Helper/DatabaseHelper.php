<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Helper/DatabaseHelper.php
 * 
 */

App::uses('AppHelper', 'View/Helper');

class DatabaseHelper extends AppHelper {
  public $helpers = array('Html', 'Form');
    
  public function databaseServerTypeLabel($value) {
    
    switch ($value) {
      case 1:
        $db_type = _('Oracle Database 11g');
        $html = "$db_type";
        break;
      case 2:
        $db_type = __('IBM DB2');
        $html = "$db_type";
        break;
      case 3:
        $db_type = __('SQL Server');
        $html = "$db_type";
        break;
      case 4:
        $db_type = __('Oracle MySQL');
        $html = "$db_type";
        break;
      default:
        $db_type = 'UNKNOWN: ' . $value;
        $html = "$db_type";
        break;
    }

    return $html;
  }
  
  
  public function databaseServerEnvLabel($value) {
    
    switch ($value) {
      case 1:
        $db_env = _('Development');
        $html = "$db_env";
        break;
      case 2:
        $db_env = __('QA');
        $html = "$db_env";
        break;
      case 3:
        $db_env = __('Stage');
        $html = "$db_env";
        break;
      case 4:
        $db_env = __('Production');
        $html = "$db_env";
        break;
      case 5:
        $db_env = __('Disaster Recovery');
        $html = "$db_env";
        break;
      default:
        $db_env = 'UNKNOWN: ' . $value;
        $html = "$db_env";
        break;
    }

    return $html;
  }
  
  
  public function databaseDeployRoute($project_id) {
       
    $html = "Help";

    return $html;
  }
  
}