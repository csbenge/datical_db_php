<?php

/* /app/View/Helper/ButtonHelper.php (using other helpers) */

App::uses('AppHelper', 'View/Helper');

class RoleHelper extends AppHelper {
  public $helpers = array('Html', 'Form');

  public function getPermText($perm) {
    if ($perm == 1)
      return "Yes";
    else
      return "No";
  }
  
}