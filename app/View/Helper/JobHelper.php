<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Helper/JobHelper.php
 * 
 */

App::uses('AppHelper', 'View/Helper');

require_once(APP . 'Console/Command' . DS . 'tdcron.php');

class JobHelper extends AppHelper {
  public $helpers = array('Html', 'Form');
  
  public function jobNextRun ($cronspec) {
    $nextrun = tdCron::getNextOccurrence($cronspec, time()+60);
    return $nextrun;
  }
  
  public function jobNextRunPrint ($nextrun) {
    return strftime('%c', $nextrun);
  }
  
  public function jobStatusLabel($value) {
    
    switch ($value) {
      case 0:
        $status = __('Off');
        $html = "<span class='label label'>$status</span>";
        break;
      case 1:
        $status =__('Ready');
        $html = "<span class='label label-info'>$status</span>";
        break;
      case 2:
        $status = __('Running');
        $html = "<span class='label label-success'>$status</span>";
        break;
      case 3:
        $status = __('Failed');
        $html = "<span class='label label-important'>$status</span>";
        break;
    }

    return $html;
  }
  
  public function jobNotifyLabel($value) {
    
    switch ($value) {
      case 0:
        $status = __('None');
        $html = "$status";
        break;
      case 1:
        $status =__('Email on Run');
        $html = "$status";
        break;
      case 2:
        $status = __('Email on Success');
        $html = "$status";
        break;
      case 3:
        $status = __('Email on Fail');
        $html = "$status";
        break;
    }

    return $html;
  }
  
}