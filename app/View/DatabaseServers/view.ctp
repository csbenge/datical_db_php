<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/DatabaseServers/view.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="jobs form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <fieldset>
        <legend><b><?php echo __('DatabaseServer'); ?>: <?php echo h($databaseserver['DatabaseServer']['dbsvr_name']); ?></b></legend>
      </fieldset>

      <div class="container">
  
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Name')); ?></div>
          <div class="span10"><?php echo $databaseserver['DatabaseServer']['dbsvr_name']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
          <div class="span10"><?php echo $databaseserver['DatabaseServer']['dbsvr_desc']; ?></div>
        </div>

        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Type')); ?></div>
          <div class="span10"><?php echo $databaseserver['DatabaseServer']['dbsvr_type']; ?></div>
        </div>
        
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Host')); ?></div>
          <div class="span10"><?php echo $databaseserver['DatabaseServer']['dbsvr_host']; ?></div>
        </div>
        
        <div class="row">
          <div class="span2"><?php echo $this->Html->tag('b', __('Port')); ?></div>
          <div class="span10"><?php echo $databaseserver['DatabaseServer']['dbsvr_port']; ?></div>
        </div>
  
        <div class="row">
          <?php echo $this->Html->link(__('Back'),"javascript:history.back()"); ?>
        </div>

      </div>
      
    </div>
  </div>
</div>