<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/DatabaseServers/index.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="row">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Session->flash(); ?>
</div>

<div class="container-fluid center">
  <div class="databaseservers index">
  
     <div class="row" style="vertical-align: middle;">
      <div class="span9">
        <fieldset>
          <legend><b><?php echo __('Database Servers'); ?></b></legend>
        </fieldset>
      </div>
      <div class="span3" style="text-align: right;">
        <br/>
          <p><?php echo $this->Button->addButton(__('Add Server')); ?></p>
      </div>
  
      <table class="table table-hover table-bordered table-condensed table-striped">
        <tr>
       <!--   <th style="background-color: #e5e5e5"><?php echo $this->Paginator->sort('id');?></th> -->
          <th style="background-color: #e5e5e5;width: 200px"><?php echo $this->Paginator->sort('dbsvr_name', __('Name'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('dbsvr_desc', __('Description'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('dbsvr_type', __('Type'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('dbsvr_host', __('Host'));?></th>
          <th style="background-color: #e5e5e5;width: 100px"><?php echo $this->Paginator->sort('dbsvr_port', __('Port'));?></th>
          <th class="actions" style="text-align: center;background-color: #e5e5e5;width: 100px"><?php echo __('Actions');?></th>
        </tr>
        <?php
          $i = 0;
          foreach ($databaseservers as $server): ?>
          <tr>
         <!--   <td><?php echo h($server['Database']['id']); ?></td> -->
            <td><?php echo $this->Html->link($server['DatabaseServer']['dbsvr_name'], array('action' => 'view', $server['DatabaseServer']['id'])); ?></td>
            <td><?php echo h($server['DatabaseServer']['dbsvr_desc']); ?></td>
            <td><?php echo $this->Database->databaseServerTypeLabel($server['DatabaseServer']['dbsvr_type']); ?></td>
            <td><?php echo h($server['DatabaseServer']['dbsvr_host']); ?></td>
            <td><?php echo h($server['DatabaseServer']['dbsvr_port']); ?></td>
            <td class="actions" style="text-align: center;">
              <?php echo $this->Button->editButton($server['DatabaseServer']['id']); ?>
              <?php echo $this->Button->deleteButton($server['DatabaseServer']['id'], $server['DatabaseServer']['dbsvr_name']); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>

    <p>
    <center>
    <?php
      echo $this->Paginator->counter(array(
      'format' => __('Page {:page} of {:pages}, showing {:current} database servers out of {:count} total, starting on server {:start}, ending on {:end}')
      ));
    ?>
    </p>
  
    <div class="paging">
      <?php
        echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
      ?>
    </div>
    </center>

  </div>
  
</div>
