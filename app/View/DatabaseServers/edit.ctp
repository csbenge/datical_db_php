<?php
/**
 *
 * Datical Open Source Integraton Platform
 *
 * Datical(tm) : Open Source Integration Platform (http://cakephp.org)
 * Copyright 2012-2014, Datical, Inc. (http://www.datical.com)
 *
 * @file: app/View/Jobs/edit.ctp
 * 
 */
  
  $this->start('sidebar');
  $this->assign('sidebar', '');
  echo $this->element('sidebar/databases');
  $this->end();
?>

<br/>

<div class="container-fluid center">
  <div class="datbaseservers form">
    <div class="row well" style="vertical-align: middle;">
      <?php echo $this->Session->flash('auth'); ?>
      <?php echo $this->Form->create('DatabaseServer'); ?>
          <fieldset>
            <legend><b><?php echo __('Edit Server'); ?></b></legend>

             <div class="container">
              
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Name')); ?></div>
                <div class="span10"><?php echo $this->Form->input('dbsvr_name', array('label' => false, 'autofocus'=>'autofocus')); ?></div>
              </div>
              
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Description')); ?></div>
                <div class="span10"><?php echo $this->Form->input('dbsvr_desc', array('label' => false)); ?></div>
              </div>
 
              <?php $dbsvr_type = $this->request->data['DatabaseServer']['dbsvr_type']; ?>
              <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Type')); ?></div>
                <div class="span10"><?php echo $this->Form->input('dbsvr_type', array(
                     'label' => false,
                     'type' => 'select',
                     'selected' => $dbsvr_type,
                     'style'=>'width:200px;',
                     'options' => array(
                         '1' => __('Oracle Database 11g'),
                         '2' => __('IBM DB2'),
                         '3' => __('SQL Server'),
                         '4' => __('Oracle MySQL')                      
                       )
                     )
                   ); ?>
                </div>
              </div>
 
             <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Host')); ?></div>
                <div class="span10"><?php echo $this->Form->input('dbsvr_host', array('label' => false)); ?></div>
              </div>
             
             <div class="row">
                <div class="span2"><?php echo $this->Html->tag('b', __('Port')); ?></div>
                <div class="span10"><?php echo $this->Form->input('dbsvr_port', array('label' => false)); ?></div>
              </div>
             
            </div>

          </fieldset>
      <?php echo $this->Form->end(__('Update Server')); ?>
      <?php echo $this->Html->link(__('Cancel'),"javascript:history.back()"); ?>
    </div>
  </div>
</div>